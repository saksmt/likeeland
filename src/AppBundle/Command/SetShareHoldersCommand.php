<?php

namespace AppBundle\Command;

use AppBundle\Entity\TrinarNode;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class SetShareHoldersCommand extends ContainerAwareCommand
{
    public function configure()
    {
        $this
            ->setName('app:shareholders:install')
            ->addArgument('pathToShareholdersDump')
        ;
    }

    public function execute(InputInterface $in, OutputInterface $out)
    {
        $out->writeln('Loading shareholders...');
        $em = $this->getContainer()->get('doctrine')->getManager();
        $dumpedUsers = unserialize(file_get_contents(
            $in->getArgument('pathToShareholdersDump')
        ));
        $out->writeln('Loaded!');
        $persistenceList = [];
        $um = $this->getContainer()->get('fos_user.user_manager');
        $out->writeln('Setting roles to shareholders...');
        foreach ($dumpedUsers as $shareholder) {
            $user = $um->findUserByUsername($shareholder);
            if (!isset($user)) continue;
            $user->addRole('ROLE_SHAREHOLDER');
            $persistenceList[] = $user;
            if (count($persistenceList) > 100) {
                foreach ($persistenceList as $obj) {
                    $em->persist($obj);
                }
                $persistenceList = [];
                $em->flush();
            }
        }
        if (count($persistenceList)) {
            foreach ($persistenceList as $obj) {
                $em->persist($obj);
            }
            $em->flush();
        }
        $out->writeln('Done!');
    }
}