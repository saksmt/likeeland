<?php

namespace AppBundle\Command;

use AppBundle\Entity\TrinarNode;
use AppBundle\Entity\User;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class RestoreTrinarCommand extends ContainerAwareCommand
{
    const MAX_PERSIST_QUEUE = 100;
    private $persistenceList = [];

    /**
     * @var ObjectManager
     */
    private $em;

    public function configure()
    {
        $this
            ->setName('app:restore:trinar')
            ->addArgument('pathToUserSet')
        ;
    }

    public function execute(InputInterface $in, OutputInterface $out)
    {
        $this->em = $this->getContainer()->get('doctrine.orm.entity_manager');
        $um = $this->getContainer()->get('fos_user.user_manager');
        $tm = $this->getContainer()->get('trinar.manager');

        $out->write('Loading user set...');
        $set = unserialize(file_get_contents($in->getArgument('pathToUserSet')));
        $out->writeln('Done!');

        $out->write('Adding to trinar...');
        foreach ($set as $username) {
            /** @var User $user */
            $user = $um->findUserByUsername($username);
            if (!isset($user)) {
                continue;
            }
            $trinar = new TrinarNode();
            $trinar->setUser($user);
            $this->persistenceList = array_merge(
                $this->persistenceList,
                $tm->registerReferral($user)
            );
            $this->save();
        }
        $this->save(true);
        $out->writeln('Done!');
    }

    private function save($force = false)
    {
        $listSize = count($this->persistenceList);
        if (($force && $listSize) || $listSize > self::MAX_PERSIST_QUEUE) {
            foreach ($this->persistenceList as $element) {
                $this->em->persist($element);
            }
            $this->em->flush();
        }
    }
}