<?php

namespace AppBundle\Command;

use AppBundle\Entity\House;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class RestoreHousesCommand extends ContainerAwareCommand
{
    public function configure()
    {
        $this
            ->setName('app:restore:houses')
            ->addArgument('pathToHousesDump')
        ;
    }

    public function execute(InputInterface $in, OutputInterface $out)
    {
        $houses = json_decode(file_get_contents($in->getArgument('pathToHousesDump')), true);
        $persister = $this->getContainer()->get('persister.factory')->createPersister();
        $em = $this->getContainer()->get('doctrine')->getManager();
        $typeRepository = $em->getRepository('AppBundle:HouseType');
        $userRepository = $em->getRepository('AppBundle:User');
        $out->write('Restoring');
        foreach ($houses as $houseData) {
            $type = $typeRepository->find($houseData['house_type_id']);
            $user = $userRepository->find($houseData['user_id']);
            for ($i = 0; $i < $houseData['q']; $i++) {
                $house = new House();
                $house
                    ->setOwner($user)
                    ->setType($type)
                ;
                if ($persister->persistMany([$house, $user, $type])) {
                    $out->write('.');
                }
            }
        }
        if ($persister->flush()) {
            $out->write('.');
        }
        $out->writeln('Done!');
    }
}