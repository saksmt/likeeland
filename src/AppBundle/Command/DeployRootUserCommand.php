<?php

namespace AppBundle\Command;

use AppBundle\Entity\TrinarNode;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class DeployRootUserCommand extends ContainerAwareCommand
{
    public function configure()
    {
        $this->setName('app:deploy');
    }

    public function execute(InputInterface $in, OutputInterface $out)
    {
        $um = $this->getContainer()->get('fos_user.user_manager');
        if ($um->findUserByUsername('root') !== null) {
            $out->writeln('User had already been created!');
            return;
        }
        $userPassword = $this->generatePassword();
        $user = $um->createUser()
            ->setUsername('root')
            ->setEmail('root@likeeland.com')
            ->setPlainPassword($userPassword)
            ->setRoles(['ROLE_ADMIN'])
            ->setEnabled(true)
            ->setName('Администратор')
        ;
        $trinar = new TrinarNode();
        $trinar->setUser($user);
        $this->getContainer()->get('doctrine.orm.entity_manager')->persist($trinar);
        $um->updateUser($user);
        $out->writeln('Successfully created root user with password "' . $userPassword . '"');
    }

    private function generatePassword()
    {
        $sym = array_merge(
            range(0, 9),
            range('a', 'z'),
            range('A', 'Z')
        );
        $result = '';
        for ($i = 0; $i < 8; $i++) {
            shuffle($sym);
            $result .= $sym[0];
        }
        return $result;
    }
}