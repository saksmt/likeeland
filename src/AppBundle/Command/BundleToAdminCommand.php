<?php

namespace AppBundle\Command;

use AppBundle\Entity\TrinarNode;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class BundleToAdminCommand extends ContainerAwareCommand
{
    public function configure()
    {
        $this
            ->setName('app:admin:refer');
    }

    protected function get($name)
    {
        return $this->getContainer()->get($name);
    }

    public function execute(InputInterface $in, OutputInterface $out)
    {
        $out->write('Processing...');
        $em = $this->getContainer()->get('doctrine')->getManager();
        $tm = $this->getContainer()->get('trinar.manager');
        $users = $em
            ->getRepository('AppBundle:User')->findNotBundled();
        $persistenceList = [];
        foreach ($users as $user) {
            $trinar = new TrinarNode();
            $trinar->setUser($user);
            $persistenceList = array_merge($persistenceList, $tm->registerReferral($user));
            if (count($persistenceList) > 100) {
                foreach ($persistenceList as $obj) {
                    $em->persist($obj);
                }
                $persistenceList = [];
                $em->flush();
            }
        }
        if (count($persistenceList)) {
            foreach ($persistenceList as $obj) {
                $em->persist($obj);
            }
            $em->flush();
        }
        $out->writeln('Done!');
    }
}