<?php

namespace AppBundle\Command;

use AppBundle\Entity\Item;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class InitializeItemsCommand extends ContainerAwareCommand
{
    private static $items = [
        [ 'title' => 'Палка', 'price' => 1 ],
        [ 'title' => 'Мина', 'price' => 1 ],
    ];

    public function configure()
    {
        $this->setName('app:init:items');
    }

    public function execute(InputInterface $in, OutputInterface $out)
    {
        if (count(
                $this->getContainer()
                    ->get('doctrine.orm.entity_manager')
                    ->getRepository('AppBundle:Item')
                    ->findAll()
            ) > 0) {
            $out->writeln('Items have already been initialized!');
            return;
        }
        $persister = $this->getContainer()->get('persister.factory')->createPersister();
        foreach (self::$items as $itemData) {
            $persister->persist((new Item())
                ->setTitle($itemData['title'])
                ->setPrice($itemData['price']));
        }
        $persister->flush();
        $out->writeln('Done!');
    }
}