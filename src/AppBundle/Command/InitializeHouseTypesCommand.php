<?php

namespace AppBundle\Command;

use AppBundle\Entity\HouseType;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class InitializeHouseTypesCommand extends ContainerAwareCommand
{
    private static $houseTypes = [
        ['title' => 'Поместье', 'image' => '1', 'price' => 0],
        ['title' => 'Вотчина', 'image' => '2', 'price' => 0],
        ['title' => 'Дворянский феод', 'image' => '3', 'price' => 0],
        ['title' => 'Княжеский удел', 'image' => '4', 'price' => 0],
    ];

    public function configure()
    {
        $this->setName('app:init:houseTypes');
    }

    public function execute(InputInterface $in, OutputInterface $out)
    {
        $em = $this->getContainer()->get('doctrine.orm.entity_manager');
        if (count($em->getRepository('AppBundle:HouseType')->findAll())) {
            $out->writeln('House types already exists!');
            $out->writeln('Exiting...');
            return;
        }
        foreach (static::$houseTypes as $houseType) {
            $em->persist((new HouseType())
                ->setTitle($houseType['title'])
                ->setImageName($houseType['image'])
                ->setPrice($houseType['price'])
            );
        }
        $em->flush();
        $out->writeln('Done!');
    }
}