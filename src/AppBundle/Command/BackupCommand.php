<?php

namespace AppBundle\Command;

use AppBundle\Entity\User;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\HttpFoundation\File\File;

class BackupCommand extends ContainerAwareCommand
{
    const COUNTER_MAX = 100;

    public function configure()
    {
        $this
            ->setName('app:backup')
            ->addArgument('pathToUsersDump')
        ;
    }

    public function execute(InputInterface $in, OutputInterface $out)
    {
        ini_set('memory.limit', 0);
        $em = $this->getContainer()->get('doctrine')->getManager();
        $um = $this->getContainer()->get('fos_user.user_manager');
        $out->write('Loading users...');
        $users = $em->getRepository('AppBundle:User')->createQueryBuilder('u')
            ->leftJoin('u.referral', 'r')
            ->select('u.id')
            ->addSelect('r.username')
            ->addSelect('u.balance')
            ->addSelect('u.failViews')
            ->addSelect('u.skype')
            ->addSelect('u.name')
            ->addSelect('u.roles')
            ->addSelect('u.enabled')
            ->addSelect('u.email')
            ->addSelect('u.password')
            ->addSelect('u.salt')
            ->addSelect('u.successViews')
            ->addSelect('u.confirmationToken')->getQuery()->getResult();
        $out->writeln('Done!(Loaded ' . count($users) . ' users)');
        $out->write('Serializing dump...');
        file_put_contents($in->getArgument('pathToUsersDump'), serialize($users));
        $out->writeln('Done!');
    }
}
