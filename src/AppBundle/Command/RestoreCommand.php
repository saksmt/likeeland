<?php

namespace AppBundle\Command;

use AppBundle\Entity\User;
use FOS\UserBundle\Model\UserManagerInterface;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class RestoreCommand extends ContainerAwareCommand
{
    private $users;
    private $trinarDefinition;
    /**
     * @var OutputInterface
     */
    private $out;
    const MAX_COUNTER = 100;

    public function configure()
    {
        $this
            ->addArgument('pathToUserFile')
            ->addArgument('pathToTrinarFile')
            ->setName('app:restore')
            ->setDescription('Restores database from dump files')
        ;
    }

    public function execute(InputInterface $input, OutputInterface $output)
    {
        $this->out = $output;
        $output->write('Loading users...');
        $this->users = unserialize(file_get_contents(
            $input->getArgument('pathToUserFile')
        ));
        $output->writeln('OK!');
        $output->write('Persisting users(Packet length: ' . intval(count($this->users) / self::MAX_COUNTER) . ')');
        $this->installUsers();
        $output->writeln('OK!');
//        $output->write('Loading trinars...');
//        $this->trinarDefinition = unserialize(file_get_contents(
//            $input->getArgument('pathToTrinarFile')
//        ));
//        $output->writeln('OK!');
//        $output->write('Persisting trinars...');
//        $this->installTrinars();
//        $output->writeln('OK!');
//        $output->writeln('Done!');
    }

    private function installUsers()
    {
        $um = $this->getContainer()->get('fos_user.user_manager');
        $em = $this->getContainer()->get('doctrine')->getManager();
        $counter = 0;
        foreach ($this->users as $userData) {
            /** @var User $user */
            $user = $um->createUser();
            $this->setUserData($user, $userData, $um);
//            $this->notify($user);
            if ($counter++ === self::MAX_COUNTER) {
                $counter = 0;
                $em->flush();
                $this->out->write('.');
                usleep(100);
            }
        }
        if ($counter > 0) {
            $em->flush();
            $counter = 0;
            $this->out->write('.');
        }
        $this->out->writeln('OK!');
        //$this->applyReferrals($um, $counter, $em);
        unset($this->users);
    }

    private function generatePassword()
    {
        $sym = array_merge(
            range(0, 9),
            range('a', 'z'),
            range('A', 'Z')
        );
        $result = '';
        for ($i = 0; $i < 8; $i++) {
            shuffle($sym);
            $result .= $sym[0];
        }
        return $result;
    }

    private function notify(User $user)
    {
        $mailer = $this->getContainer()->get('swiftmailer.mailer');
        /** @var \Swift_Message $message */
        $message = \Swift_Message::newInstance();
        try {
            $message
                ->setSubject('Likeeland: Ваш новый пароль')
                ->setFrom('noreply@likeeland.com', 'Likeeland')
                ->setTo($user->getEmail())
                ->setBody($this->getContainer()->get('templating')
                    ->render('mail/reset.txt.twig', ['user' => $user]));
            $mailer->send($message);
        } catch (\Exception $e) {}
    }

    private function installTrinars()
    {
        $list = array_unique(array_merge(
            array_map(function ($trinar) { return $trinar['me']; }, $this->trinarDefinition),
            array_map(function ($trinar) { return $trinar['n1']; }, $this->trinarDefinition)
        ));
        $tm = $this->getContainer()->get('trinar.manager');
        $um = $this->getContainer()->get('fos_user.user_manager');
        $em = $this->getContainer()->get('doctrine')->getManager();
        $persistenceList = [];
        $persist = function ($force = false) use ($em, &$persistenceList) {
            if ($force || count($persistenceList) > self::MAX_COUNTER) {
                foreach ($persistenceList as $obj) {
                    $em->persist($obj);
                }
                $em->flush();
                usleep(100);
                $persistenceList = [];
            }
        };
        foreach ($list as $username) {
            $user = $um->findUserByUsername($username);
            if (!isset($user)) {
                continue;
            }
            $persistenceList = array_merge(
                $persistenceList,
                $tm->registerReferral($user),
                [$user]
            );
            $user
                ->removeRole('ROLE_INACTIVE')
                ->addRole('ROLE_USER');
            $persist();
        }
        $persist(true);

    }

    private function setUserData(User $user, $userData, UserManagerInterface $um)
    {
        if (!isset($userData['login']) || in_array($userData['login'], [
                'MCrenny',
                'tan',
                'stella'
            ]) || in_array($userData['email'], [
                'kuzfund@gmail.com'
            ]) || empty($userData['login']))
            return;
        $user
            ->setUsername($userData['login'])
            ->setBalance($userData['bal'])
            ->setPlainPassword($this->generatePassword())
            ->setEnabled(true)
            ->setRoles(['ROLE_INACTIVE'])
            ->setEmail($userData['email'])
        ;
        $um->updateUser($user, false);
        $this->notify($user);
//        $user
//            ->setEmail($userData['email'])
//            ->setUsername($userData['username'])
//            ->setPassword($userData['password'])
//            ->setEnabled($userData['enabled'])
//            ->setRoles($userData['roles'])
//            ->setPlainPassword($this->generatePassword())
//            ->setBalance($userData['balance'])
//            ->setFailViews($userData['failViews'])
//            ->setSuccessViews($userData['successViews'])
//            ->setConfirmationToken($userData['confirmationToken'])
//            ->setSkype($userData['skype'])
//            ->setName($userData['name'])
//            ->setSalt($userData['salt'])
//        ;
    }

    /**
     * @param $um
     * @param $counter
     * @param $em
     */
    private function applyReferrals($um, $counter, $em)
    {
        $this->out->write('Setting referrers(Packet length: ' . intval(count($this->users) / self::MAX_COUNTER) . ')');
        foreach ($this->users as $userData) {
            $user = $um->findUserByUsername($userData['username']);
            $referral = $um->findUserByUsername($userData['referral']);
            $user->setReferral($referral);
            $um->updateUser($user, false);
            if ($counter++ > self::MAX_COUNTER) {
                $em->flush();
                $counter = 0;
                $this->out->write('.');
                usleep(100);
            }
        }
        if ($counter > 0) {
            $em->flush();
            $this->out->write('.');
        }
    }
}