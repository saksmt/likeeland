<?php

namespace AppBundle\Command;

use AppBundle\Entity\Item;
use AppBundle\Entity\Project;
use AppBundle\Entity\User;
use AppBundle\Util\Enum\ProjectStatus;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class ReturnMoneyToInvestorsCommand extends ContainerAwareCommand
{

    public function configure()
    {
        $this->setName('app:task:returnMoney');
    }

    public function execute(InputInterface $in, OutputInterface $out)
    {
        $failProjects = $this
            ->getContainer()
            ->get('doctrine')
            ->getRepository('AppBundle:Project')
            ->findAllOutdated()
        ;
        $persister = $this->getContainer()->get('persister.factory')->createPersister();
        $em = $this->getContainer()->get('doctrine.orm.entity_manager');
        foreach($failProjects as $project) {
            if ($project->getStatus() == ProjectStatus::FAILED) {
                foreach($project->getPayments() as $payment) {
                    /** @var User $investor */
                    $investor = $payment->getInvestor();
                    $investor->setBalance($investor->getBalance() + $payment->getAmount());
                    $persister->persist($investor);
                }
            }
            $persister->flush();
            $em->remove($project);
            $em->flush();
        }
    }
}