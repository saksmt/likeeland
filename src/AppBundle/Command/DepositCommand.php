<?php

namespace AppBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class DepositCommand extends ContainerAwareCommand
{
    public function configure()
    {
        $this
            ->setName('app:deposit:pay')
            ->setDescription('Check for unpaid deposits and pay for them');
    }

    public function execute(InputInterface $in, OutputInterface $out)
    {
        $persister = $this->getContainer()->get('persister.factory')->createPersister();
        $deposits = $this->getContainer()->get('doctrine')->getManager()
            ->getRepository('AppBundle:Deposit')->findAllNeedPayment();
        $progress = new ProgressBar($out, count($deposits));
        $progress->start();
        foreach ($deposits as $deposit) {
            $user = $deposit->getInvestor();
            $user->setBalance($user->getBalance() + $deposit->getResultAmount());
            $deposit->setPaid();
            if ($persister->persistMany([$user, $deposit])) {
                $progress->setMessage('Flushing...');
            }
            $progress->advance();
        }
        $persister->flush();
        $progress->finish();
    }
}