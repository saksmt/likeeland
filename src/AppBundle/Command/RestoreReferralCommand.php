<?php

namespace AppBundle\Command;

use AppBundle\Entity\User;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\HttpFoundation\File\File;

class RestoreReferralCommand extends ContainerAwareCommand
{
    const COUNTER_MAX = 100;

    public function configure()
    {
        $this
            ->setName('app:restore:referral')
            ->addArgument('pathToUsersDump')
        ;
    }

    public function execute(InputInterface $in, OutputInterface $out)
    {
        ini_set('memory.limit', 0);
        $em = $this->getContainer()->get('doctrine')->getManager();
        $um = $this->getContainer()->get('fos_user.user_manager');
        $out->write('Loading active users...');
        $activeUsernames = array_map(function (User $user) {
            return $user->getUsername();
        }, $em->getRepository('AppBundle:User')->findActive());
        $out->writeln('Done!');
        $counter = 0;
        $flush = function (&$counter, $force = false) use ($em, $out) {
            if (($force && $counter) || $counter >= self::COUNTER_MAX) {
                $counter = 0;
                $em->flush();
                $out->write('.');
            }
        };
        $out->write('Loading dump...');
        $dumpedUsers = @unserialize(
            base64_decode(file_get_contents($in->getArgument('pathToUsersDump')))
        );
        $log = [];
        $out->writeln('Done!');
        $out->write('Applying referrals(Packets: ' .
            ceil((count($dumpedUsers) - count($activeUsernames)) / self::COUNTER_MAX)
            . ')');
        foreach ($dumpedUsers as $dumpedUser) {
            $login = base64_decode($dumpedUser['login']);
            if (in_array($login, $activeUsernames)) {
                continue;
            }
            $refer = base64_decode($dumpedUser['refer']);
            try {
                /** @var User $user */
                $user = $um->findUserByUsername($login);
                /** @var User $referral */
                $referral = $um->findUserByUsername($refer);
                if (isset($referral)) {
                    $user->setReferral($referral);
                    $em->persist($user);
                    $counter++;
                    $flush($counter);
                }
            } catch (\Exception $e) {
                $log[] = $login;
            }
        }
        file_put_contents('error.log', serialize($log));
        $flush($counter, true);
        $out->writeln('Done!');
    }
}