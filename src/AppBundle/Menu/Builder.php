<?php

namespace AppBundle\Menu;

use Knp\Menu\FactoryInterface;
use Knp\Menu\ItemInterface;
use Knp\Menu\MenuItem;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;

class Builder
{
    /**
     * @var FactoryInterface
     */
    private $factory;

    /**
     * @var AuthorizationCheckerInterface
     */
    private $authChecker;

    /**
     * @var TokenStorageInterface
     */
    private $tokenStorage;

    /**
     * @var MenuItem
     */
    private $menu;

    /**
     * @var MenuItem
     */
    private $adminMenu;

    /**
     * @var MenuItem
     */
    private $exchangeMenu;

    /**
     * @var MenuItem
     */
    private $houseManagerMenu;

    /**
     * @var MenuItem
     */
    private $currentBaseMenu;

    public function __construct(
        FactoryInterface $factory,
        TokenStorageInterface $tokenStorage,
        AuthorizationCheckerInterface $checker
    )
    {
        $this->factory = $factory;
        $this->authChecker = $checker;
        $this->tokenStorage = $tokenStorage;
    }

    public function createLeftMenuPart(RequestStack $requestStack)
    {
        $this->menu = $this->factory->createItem('root');
        $this->setCurrentBaseMenu($this->menu);
        if ($this->authChecker->isGranted('ROLE_USER')) {
            $this->buildAuthorizedMenu();
        } else {
            $this->buildUnauthorizedMenu();
        }
        return $this->menu;
    }

    public function createRightMenuPart(RequestStack $requestStack)
    {
        $rightMenu = $this->factory->createItem('root');
        if ($this->authChecker->isGranted('ROLE_USER')) {
            $rightMenu->addChild('Выйти', ['route' => 'fos_user_security_logout']);
        } else {
            $rightMenu->addChild('Авторизация', ['route' => 'fos_user_security_login']);
        }
        return $rightMenu;
    }

    private function buildAuthorizedMenu()
    {
        $this->buildUserMenu();
        if ($this->authChecker->isGranted('ROLE_ADMIN')) {
            $this->adminMenu = $this->menu->addChild('Администрирование');
            $this->buildAdministrationMenu();
        }
    }

    private function buildUnauthorizedMenu()
    {
        $this
            ->addMenuItem('Присоединиться', ['route' => 'fos_user_registration_register'])
            ->addMenuItem('Забыл пароль', ['route' => 'fos_user_resetting_request']);
    }

    private function buildHouseManagementMenu()
    {
        $this->houseManagerMenu = $this->menu->addChild('Владения', ['uri' => 'javascript:void(0)']);
        $this
            ->setCurrentBaseMenu($this->houseManagerMenu)
            ->addMenuItem('Приобретения', ['route' => 'purchases'])
            ->addMenuItem('Просмотр владений', ['route' => 'houseList'])
            ->addMenuItem('Магазин', ['route' => 'itemShop'])
            ->setCurrentBaseMenu($this->menu)
        ;
        return $this;
    }

    private function buildExchangeMenu()
    {
        $this->exchangeMenu = $this->menu->addChild('Биржа', ['uri' => 'javascript:void(0)']);
        $this
            ->setCurrentBaseMenu($this->exchangeMenu)
            ->addMenuItem('Выставить резиденции на продажу', ['route' => 'sellHouses'])
            ->addMenuItem('Мои предложения', ['route' => 'myDeals'])
            ->addMenuItem('Список предложений', ['route' => 'listDeals', 'routeParameters' => [
                'sort' => 'd.price',
                'direction' => 'asc'
            ]])
            ->setCurrentBaseMenu($this->menu)
        ;
        return $this;
    }

    private function buildUserMenu()
    {
        $this
            ->addMenuItem('Партнеры', ['route' => 'userList'])
//            ->addMenuItem('Видео', ['route' => 'video'])
            ->addMenuItem('Профиль', ['route' => 'profile'])
            ->addMenuItem('Тринар', ['route' => 'trinar'])
            ->addMenuItem('Инвестиции', ['route' => 'depositList'])
                ->restrict('ROLE_ADMIN')
            ->buildHouseManagementMenu()
            ->buildExchangeMenu()
            ->buildStartupMenu()
            ->buildBankMenu()
            ->addMenuItem('Акции', ['route' => 'shareView'])->restrict('ROLE_SHAREHOLDER')
        ;
    }

    private function buildAdministrationMenu()
    {
        $this
            ->setCurrentBaseMenu($this->adminMenu)
            ->addMenuItem('Заявки на вывод', ['route' => 'paymentsOut'])
//            ->addMenuItem('Просмотры видео', ['route' => 'videoViews'])
//            ->addMenuItem('Настройка видео', ['route' => 'video_create'])
            ->addMenuItem('Добавить дома пользователям', ['route' => 'addHouse'])
            ->addMenuItem('Создать сообщение резиденциям', ['route' => 'createMessage'])
            ->setCurrentBaseMenu($this->menu)
        ;
        return $this;
    }

    private function buildBankMenu()
    {
        $this
            ->setCurrentBaseMenu($this->menu->addChild('Банк'))
            ->addMenuItem('Обмен валют', ['route' => 'bankIndex'])
            ->addMenuItem('Администрирование', ['route' => 'bankAdmin'])
                ->restrict('ROLE_ADMIN')
            ->setCurrentBaseMenu($this->menu)
        ;
        return $this;

    }

    private function buildStartupMenu()
    {
        $startupMenu = $this->menu->addChild('Старт Ап');
        $this
            ->setCurrentBaseMenu($startupMenu)
            ->addMenuItem('Создать бизнес идею', ['route' => 'startupCreate'])
            ->addMenuItem('Список бизнес идей', ['route' => 'startupList'])
            ->addMenuItem('История', ['route' => 'startupHistory'])
            ->addMenuItem('Администрирование', ['route' => 'startupAdminList'])
                ->restrict('ROLE_ADMIN')
            ->setCurrentBaseMenu($this->menu)
        ;
        return $this;
    }

    private function addMenuItem($name, array $options)
    {
        $this->currentBaseMenu->addChild($name, $options);
        return $this;
    }

    private function restrict($expression)
    {
        if (!$this->authChecker->isGranted($expression)) {
            $lastItem = $this->currentBaseMenu->getLastChild();
            $this->currentBaseMenu->removeChild($lastItem);
        }
        return $this;
    }

    private function setCurrentBaseMenu(ItemInterface $baseMenu)
    {
        $this->currentBaseMenu = $baseMenu;
        return $this;
    }
}