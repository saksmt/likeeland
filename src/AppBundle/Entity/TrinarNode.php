<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\PersistentCollection;

/**
 * Class TrinarNode
 * @package AppBundle\Entity
 * @ORM\Entity()
 * @ORM\Table(name="tbl_trinar")
 */
class TrinarNode
{
    const NODE_CHILDREN_MAX = 3;
    const NODE_CHILD_LEVELS_MAX = 6;

    /**
     * @ORM\Id()
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\OneToOne(targetEntity="AppBundle\Entity\User", inversedBy="trinar")
     */
    private $user;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\TrinarNode", inversedBy="children")
     * @ORM\JoinColumn(name="parent", referencedColumnName="id")
     */
    private $parent;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\TrinarNode", mappedBy="parent")
     */
    private $children;

    /**
     * @ORM\Column(type="bigint")
     */
    private $weight = 0;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->children = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set weight
     *
     * @param integer $weight
     * @return TrinarNode
     */
    public function setWeight($weight)
    {
        $this->weight = $weight;

        return $this;
    }

    /**
     * Get weight
     *
     * @return integer
     */
    public function getWeight()
    {
        return $this->weight;
    }

    /**
     * Set user
     *
     * @param User $user
     * @return TrinarNode
     */
    public function setUser(User $user = null)
    {
        $this->user = $user;
        $user->setTrinar($this);

        return $this;
    }

    /**
     * Get user
     *
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set parent
     *
     * @param TrinarNode $parent
     * @return TrinarNode
     */
    public function setParent(TrinarNode $parent = null)
    {
        $this->parent = $parent;

        return $this;
    }

    /**
     * Get parent
     *
     * @return TrinarNode
     */
    public function getParent()
    {
        return $this->parent;
    }

    /**
     * Add children
     *
     * @param TrinarNode $node
     * @return TrinarNode[]
     */
    public function addChild(TrinarNode $node)
    {
        if (count($this->children) < static::NODE_CHILDREN_MAX) {
            $this->children[] = $node;
            $node->setParent($this);
            $this->weight++;
            return [$this, $node];
        } else {
            if ($this->children instanceof PersistentCollection) {
                /** @var PersistentCollection $children */
                $this->children->initialize();
            }
            /** @var TrinarNode[] $children */
            $children = $this->children->toArray();
            $targetWeight = $this->getTargetChildWeight();
            $children = array_filter($children, function (TrinarNode $node) use ($targetWeight) {
                return $node->weight < $targetWeight;
            });
            usort($children, function (TrinarNode $node1, TrinarNode $node2) use ($targetWeight) {
                $sorting = (($targetWeight - $node1->weight) - ($targetWeight - $node2->weight));
                if (!$sorting) {
                    $sorting = ($node1->weight - $node2->weight);
                    if (!$sorting) {
                        $sorting = ($node1->id - $node2->id);
                    }
                }
                if ($sorting > 0) {
                    $sorting = 1;
                } elseif ($sorting < 0) {
                    $sorting = -1;
                }
                return $sorting;
            });
            foreach ($children as $childNode) {
                $this->weight++;
                return array_merge($childNode->addChild($node), [$this]);
            }
            return [];
        }
    }

    private function getTargetChildWeight()
    {
        $levels = array_map(function (TrinarNode $node) {
            return $node->getChildLevelCount();
        }, $this->children->toArray());
        return static::weightOf(min($levels));
    }

    /**
     * Remove children
     *
     * @param TrinarNode $children
     */
    public function removeChild(TrinarNode $children)
    {
        $this->children->removeElement($children);
    }

    /**
     * Get children
     *
     * @return Collection
     */
    public function getChildren()
    {
        return $this->children;
    }

    public function getChildLevelCount()
    {
        return static::levelOf($this->weight);
    }

    public static function weightOf($level)
    {
        if (!$level) {
            return 0;
        }
        return pow(self::NODE_CHILDREN_MAX, $level) + static::weightOf($level-1);
    }

    public static function levelOf($weight)
    {
        $level = 0;
        while (static::weightOf($level) < $weight) {
            $level++;
        }
        if (static::weightOf($level) == $weight) {
            $level++;
        }
        return $level;
    }

    public function getLevelOf(TrinarNode $node, $startLevel = 0)
    {
        foreach ($this->children as $child) {
            if ($node === $child) {
                return $startLevel;
            }
            if (($result = $child->getLevelOf($node, $startLevel + 1)) !== false) {
                return $result;
            }
        }
        return false;
    }
}
