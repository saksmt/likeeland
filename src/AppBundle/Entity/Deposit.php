<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class Deposit
 * @package AppBundle\Entity
 * @ORM\Entity(repositoryClass="AppBundle\Repository\DepositRepository")
 * @ORM\Table(name="tbl_deposit")
 */
class Deposit
{
    /**
     * @const int Percentage for 3 day deposit
     */
    const THREE_DAY_PERCENTAGE = 2;

    /**
     * @const int Percentage for week deposit
     */
    const WEEK_PERCENTAGE = 6;

    /**
     * @const int Percentage for month deposit
     */
    const MONTH_PERCENTAGE = 17;

    /**
     * @var int
     * @ORM\Column(type="integer", length=11)
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Id()
     */
    private $id;

    /**
     * @var float
     * @ORM\Column(type="float")
     * @Assert\GreaterThanOrEqual(value="1")
     * @Assert\NotBlank()
     */
    private $amount;

    /**
     * @var int
     * @ORM\Column(type="smallint", length=2)
     */
    private $percentage;

    /**
     * @var User
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\User", inversedBy="deposits")
     * @ORM\JoinColumn(name="investor_id", referencedColumnName="id")
     */
    private $investor;

    /**
     * @var \DateTime
     * @ORM\Column(name="end_date", type="datetime")
     */
    private $endDate;

    /**
     * @var \DateTime
     * @ORM\Column(name="start_date", type="datetime")
     */
    private $startDate;

    /**
     * @var bool
     * @ORM\Column(name="paid", type="boolean")
     */
    private $paid = false;

    /**
     * @param User $user
     * @return Deposit
     */
    public static function createQuickDeposit(User $user)
    {
        return (new static($user))
            ->setPercentage(static::THREE_DAY_PERCENTAGE)
            ->setLength(3);
    }

    /**
     * @param User $user
     * @return Deposit
     */
    public static function createForWeek(User $user)
    {
        return (new static($user))
            ->setPercentage(static::WEEK_PERCENTAGE)
            ->setLength(7);
    }

    /**
     * @param User $user
     * @return Deposit
     */
    public static function createForMonth(User $user)
    {
        return (new static($user))
            ->setPercentage(static::MONTH_PERCENTAGE)
            ->setLength(30);
    }

    private function __construct(User $user)
    {
        $this->investor = $user;
        $this->startDate = new \DateTime();
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $days
     * @return Deposit
     */
    public function setLength($days)
    {
        $endDate = clone $this->startDate;
        $this->endDate = $endDate->modify(sprintf('+ %d days', $days));
        return $this;
    }

    /**
     * @return float
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * @param float $amount
     * @return $this
     */
    public function setAmount($amount)
    {
        $this->amount = $amount;
        return $this;
    }

    /**
     * @return int
     */
    public function getPercentage()
    {
        return $this->percentage;
    }

    /**
     * @param int $percentage
     * @return $this
     */
    public function setPercentage($percentage)
    {
        $this->percentage = $percentage;
        return $this;
    }

    /**
     * @return User
     */
    public function getInvestor()
    {
        return $this->investor;
    }

    /**
     * @param User $investor
     * @return $this
     */
    public function setInvestor($investor)
    {
        $this->investor = $investor;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getEndDate()
    {
        return $this->endDate;
    }

    /**
     * @param \DateTime $endDate
     * @return $this
     */
    public function setEndDate($endDate)
    {
        $this->endDate = $endDate;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getStartDate()
    {
        return $this->startDate;
    }

    /**
     * @param \DateTime $startDate
     * @return $this
     */
    public function setStartDate($startDate)
    {
        $this->startDate = $startDate;
        return $this;
    }

    /**
     * @return bool
     */
    public function isPaid()
    {
        return $this->paid;
    }

    /**
     * @param bool $paid
     * @return $this
     */
    public function setPaid($paid = true)
    {
        $this->paid = $paid;
        return $this;
    }

    /**
     * @return float
     */
    public function getResultAmount()
    {
        return $this->amount + $this->amount * $this->percentage / 100;
    }
}