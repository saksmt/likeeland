<?php

namespace AppBundle\Entity\Util;

use Symfony\Component\HttpFoundation\File\UploadedFile;
use Util\File\FileExtensionGuesser;

/**
 * Superclass for easy image handling in entities
 * @package AppBundle\Entity\Util
 */
abstract class ImageHandler
{
    /**
     * @var string
     * Accessor postfix for field with image
     */
    protected static $imageNameAccessor = 'ImageName';

    /**
     * @var string
     * Getter for field with uploaded file
     */
    protected static $fileGetter = 'getImageFile';

    /**
     * @var string
     */
    protected static $baseUrl = '';

    /**
     * @var string[]
     * Getters for unique properties
     */
    protected static $uniqueProperties = [];

    /**
     * @return string
     */
    public function getImageUrl()
    {
        return static::$baseUrl . '/' . $this->{'get' .static::$imageNameAccessor}();
    }

    /**
     * @return string
     */
    protected static function getBasePath()
    {
        return dirname(dirname(dirname(dirname(__DIR__)))) . '/web/';
    }

    /**
     * @return string
     */
    public function getImagePath()
    {
        return static::getBasePath() . $this->getImageUrl();
    }

    /**
     * Moves image to specified folder and saves it's name in specified field
     */
    protected function saveImage()
    {
        /** @var UploadedFile $file */
        $file = $this->{static::$fileGetter}();
        $prefix = $this->getUniquePrefix();
        $fileName = md5(uniqid($prefix));
        $extension = (new FileExtensionGuesser($file))->getExtension('png');
        $fileName .= '.' . $extension;
        $file->move(static::getBasePath() . static::$baseUrl, $fileName);
        $this->{'set' . static::$imageNameAccessor}($fileName);
    }

    /**
     * @return string
     */
    private function getUniquePrefix()
    {
        $prefix = static::class;
        foreach (static::$uniqueProperties as $propertyGetter) {
            $prefix .= (string)$this->{$propertyGetter}();
        }
        return $prefix;
    }
}