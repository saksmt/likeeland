<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class Item
 * @package AppBundle\Entity
 * @ORM\Entity()
 * @ORM\Table(name="tbl_items")
 */
class Item
{
    /**
     * @ORM\Id()
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(type="float")
     * @Assert\GreaterThan(value="0")
     */
    private $price;

    /**
     * @ORM\Column(type="string", length=100)
     * @Assert\Length(max="100")
     */
    private $title;

    /**
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return float
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * @param float $price
     * @return $this
     */
    public function setPrice($price)
    {
        $this->price = $price;
        return $this;
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param string $title
     * @return $this
     */
    public function setTitle($title)
    {
        $this->title = $title;
        return $this;
    }

    public function getImageUrl()
    {
        return self::getBaseUrl() . $this->getId() . '.png';
    }

    private static function getBaseUrl()
    {
        return '/images/items/';
    }
}
