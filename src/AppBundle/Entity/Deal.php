<?php

namespace AppBundle\Entity;

use AppBundle\Util\Enum\PaymentStatus;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class Deal
 * @package AppBundle\Entity
 * @ORM\Entity(repositoryClass="AppBundle\Repository\DealRepository")
 * @ORM\Table(name="tbl_exchange")
 */
class Deal
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(name="id", type="integer")
     */
    private $id;

    /**
     * @ORM\Column(name="price", type="float")
     */
    private $price;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\House", mappedBy="deal")
     * @var Collection $houses
     */
    private $houses;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\User")
     * @ORM\JoinColumn(fieldName="seller_id", referencedColumnName="id")
     */
    private $seller;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\User")
     * @ORM\JoinColumn(fieldName="customer_id", referencedColumnName="id")
     */
    private $customer;

    /**
     * @ORM\Column(name="status", type="smallint")
     */
    private $status = PaymentStatus::PENDING;

    /**
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    public function __construct()
    {
        $this->houses = new ArrayCollection();
    }

    /**
     * @return integer
     */
    public function getQuantity()
    {
        return $this->houses->count();
    }

    /**
     * @return float
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * @param float $price
     * @return $this
     */
    public function setPrice($price)
    {
        $this->price = $price;
        return $this;
    }

    /**
     * @return integer
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param integer $status
     * @return $this
     */
    public function setStatus($status)
    {
        $this->status = $status;
        return $this;
    }

    /**
     * @return Collection
     */
    public function getHouses()
    {
        return $this->houses;
    }

    /**
     * @param Collection $houses
     * @return $this
     */
    public function setHouses(Collection $houses = null)
    {
        $this->houses = $houses;
        return $this;
    }

    public function addHouse(House $house)
    {
        $house->setDeal($this);
        $this->houses[] = $house;
        return $this;
    }

    public function removeHouse(House $house)
    {
        $house->setDeal(null);
        $this->houses->removeElement($house);
        return $this;
    }

    /**
     * @return User
     */
    public function getSeller()
    {
        return $this->seller;
    }

    /**
     * @param User $seller
     * @return $this
     */
    public function setSeller($seller)
    {
        $this->seller = $seller;
        return $this;
    }

    /**
     * @return User
     */
    public function getCustomer()
    {
        return $this->customer;
    }

    /**
     * @param User $customer
     * @return $this
     */
    public function setCustomer($customer)
    {
        $this->customer = $customer;
        return $this;
    }

    public function dismiss()
    {
        $houses = $this->houses->toArray();
        $this->status = PaymentStatus::FAIL;
        $this->houses->map(function (House $house) { $this->removeHouse($house); });
        return $houses;
    }

    public function buy(User $user)
    {
        $this
            ->setCustomer($user)
            ->setStatus(PaymentStatus::SUCCESS);
        $user->pay($this->getPrice());
        $this->getSeller()->setBalance($this->getSeller()->getBalance() + $this->getPrice());
        $houses = $this->getHouses()->toArray();
        $this->houses->map(function (House $house) {
            $house->setOwner($this->getCustomer());
            $this->removeHouse($house);
        });
        return $houses;
    }
}
