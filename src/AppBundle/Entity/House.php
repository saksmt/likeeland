<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class House
 * @package AppBundle\Entity
 * @ORM\Entity()
 * @ORM\Table(name="tbl_house")
 */
class House
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(name="id", type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\HouseType", inversedBy="houses")
     * @ORM\JoinColumn(fieldName="type_id", referencedColumnName="id")
     * @var HouseType
     */
    private $type;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\User", inversedBy="houses")
     * @ORM\JoinColumn(fieldName="owner_id", referencedColumnName="id")
     * @var User
     */
    private $owner;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Deal", inversedBy="houses")
     * @ORM\JoinColumn(fieldName="deal_id", referencedColumnName="id", nullable=true)
     */
    private $deal;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\InventoryItem", mappedBy="house")
     */
    private $items;
    
    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\HouseMessage", mappedBy="house")
     */
    private $messages;

    public function __construct()
    {
        $this->messages = new ArrayCollection();
        $this->items = new ArrayCollection();
    }

    /**
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return HouseType
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param HouseType $type
     * @return $this
     */
    public function setType(HouseType $type = null)
    {
        if (isset($this->type)) {
            $this->type->removeHouse($this);
        }
        $this->type = $type;
        if (isset($type)) {
            $type->addHouse($this);
        }
        return $this;
    }

    /**
     * @return User
     */
    public function getOwner()
    {
        return $this->owner;
    }

    /**
     * @param User $owner
     * @return $this
     */
    public function setOwner(User $owner = null)
    {
        if (isset($this->owner)) {
            $this->owner->removeHouse($this);
        }
        $this->owner = $owner;
        if (isset($owner)) {
            $owner->addHouse($this);
        }
        return $this;
    }

    /**
     * @return Deal
     */
    public function getDeal()
    {
        return $this->deal;
    }

    /**
     * @param Deal $deal
     * @return $this
     */
    public function setDeal(Deal $deal = null)
    {
        $this->deal = $deal;
        return $this;
    }

    /**
     * @return Collection
     */
    public function getItems()
    {
        return $this->items;
    }

    /**
     * @param Collection $items
     * @return $this
     */
    public function setItems(Collection $items)
    {
        $this->items = $items;
        return $this;
    }

    public function addItem(InventoryItem $item)
    {
        $this->items->add($item);
        return $this;
    }

    public function removeItem(InventoryItem $item)
    {
        $this->items->removeElement($item);
        return $this;
    }

    /**
     * Add messages
     *
     * @param HouseMessage $messages
     * @return House
     */
    public function addMessage(HouseMessage $messages)
    {
        $this->messages[] = $messages;

        return $this;
    }

    /**
     * Remove messages
     *
     * @param HouseMessage $messages
     */
    public function removeMessage(HouseMessage $messages)
    {
        $this->messages->removeElement($messages);
    }

    /**
     * Get messages
     *
     * @return Collection 
     */
    public function getMessages()
    {
        return $this->messages;
    }

    /**
     * @param int $x
     * @param int $y
     * @return bool
     */
    public function hasItemOn($x, $y)
    {
        return $this->getItemOn($x, $y) !== null;
    }

    /**
     * @param int $x
     * @param int $y
     * @return InventoryItem|null
     */
    public function getItemOn($x, $y)
    {
        $result = $this->items->filter(function (InventoryItem $item) use ($x, $y) {
            return $item->getX() == $x && $item->getY() == $y;
        })->first();
        return ($result === false ? null : $result);
    }
}
