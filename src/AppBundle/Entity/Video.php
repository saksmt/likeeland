<?php

namespace AppBundle\Entity;

use AppBundle\Util\Enum\VideoQuality;
use Doctrine\ORM\Mapping as ORM;

/**
 * @package AppBundle\Entity
 * @ORM\Entity()
 * @ORM\Table(name="tbl_videos")
 */
class Video
{
    /**
     * @ORM\Id()
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(name="base_link", type="string")
     */
    private $baseLink;

    /**
     * @ORM\Column(name="max_quality", type="smallint")
     */
    private $maxQuality = VideoQuality::Q720;

    /**
     * @ORM\Column(type="boolean")
     */
    private $current = false;

    /**
     * @ORM\Column(name="current_views", type="integer")
     */
    private $currentViews = 0;

    /**
     * @ORM\Column(name="max_views", type="integer")
     */
    private $maxViews = 2000;

    /**
     * @ORM\Column(name="update_time", type="datetime")
     */
    private $updateTime;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getBaseLink()
    {
        return $this->baseLink;
    }

    /**
     * @param string $baseLink
     * @return $this
     */
    public function setBaseLink($baseLink)
    {
        $this->baseLink = $baseLink;
        return $this;
    }

    /**
     * @return int
     */
    public function getMaxQuality()
    {
        return $this->maxQuality;
    }

    /**
     * @param int $maxQuality
     * @return $this
     */
    public function setMaxQuality($maxQuality)
    {
        $this->maxQuality = $maxQuality;
        return $this;
    }

    /**
     * @return bool
     */
    public function getCurrent()
    {
        return $this->current;
    }

    /**
     * @param bool $current
     * @return $this
     */
    public function setCurrent($current = true)
    {
        $this->current = $current;
        return $this;
    }

    /**
     * @return bool
     */
    public function isCurrent()
    {
        return $this->current;
    }


    /**
     * Set currentViews
     *
     * @param integer $currentViews
     * @return Video
     */
    public function setCurrentViews($currentViews)
    {
        $this->currentViews = $currentViews;

        return $this;
    }

    /**
     * Get currentViews
     *
     * @return integer 
     */
    public function getCurrentViews()
    {
        return $this->currentViews;
    }

    /**
     * Set maxViews
     *
     * @param integer $maxViews
     * @return Video
     */
    public function setMaxViews($maxViews)
    {
        $this->maxViews = $maxViews;

        return $this;
    }

    /**
     * Get maxViews
     *
     * @return integer 
     */
    public function getMaxViews()
    {
        return $this->maxViews;
    }

    /**
     * Set updateTime
     *
     * @param \DateTime $updateTime
     * @return Video
     */
    public function setUpdateTime($updateTime)
    {
        $this->updateTime = $updateTime;

        return $this;
    }

    /**
     * Get updateTime
     *
     * @return \DateTime 
     */
    public function getUpdateTime()
    {
        return $this->updateTime;
    }
}
