<?php

namespace AppBundle\Entity;

use AppBundle\Entity\Util\ImageHandler;
use AppBundle\Util\Enum\ProjectStatus;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Entity for startup section
 * @package AppBundle\Entity
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ProjectRepository")
 * @ORM\Table(name="tbl_projects")
 * @ORM\HasLifecycleCallbacks()
 */
class Project extends ImageHandler
{
    /**
     * @var int
     * @ORM\Id()
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(type="string", length=50)
     * @Assert\Length(max="50", groups={"create"})
     * @Assert\NotBlank(groups={"create"})
     */
    private $title;

    /**
     * @var string
     * @ORM\Column(type="text")
     * @Assert\NotBlank(groups={"create"})
     */
    private $description;

    /**
     * @var float
     * @ORM\Column(type="float")
     * @Assert\GreaterThan(value="0", groups={"create"})
     * @Assert\NotBlank(groups={"create"})
     */
    private $budget;

    /**
     * @var Collection
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Investment", mappedBy="project", cascade={"remove"})
     */
    private $payments;

    /**
     * @var \DateTime
     * @ORM\Column(name="review_date", type="datetime", nullable=true)
     */
    private $reviewDate;

    /**
     * @var int
     * @ORM\Column(name="project_time", type="smallint", length=3)
     * @Assert\LessThanOrEqual(value="999", groups={"create"})
     * @Assert\NotBlank(groups={"create"})
     */
    private $projectTime;

    /**
     * @var int
     * @ORM\Column(type="smallint", length=1)
     */
    private $status = ProjectStatus::PENDING;

    /**
     * @var User
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\User")
     * @ORM\JoinColumn(name="owner_id", referencedColumnName="id", onDelete="SET NULL")
     */
    private $owner;

    /**
     * @var string
     * @ORM\Column(name="decline_description", type="text", nullable=true)
     */
    private $declineDescription;

    /**
     * @var string
     * @ORM\Column(type="string", length=38)
     */
    private $image;

    /**
     * @var UploadedFile
     * @Assert\NotNull(groups={"create"})
     * @Assert\Image(groups={"create"})
     */
    private $imageFile;

    /**
     * @var string
     */
    protected static $imageNameAccessor = 'Image';

    /**
     * @var string
     */
    protected static $fileGetter = 'getImageFile';

    /**
     * @var string[]
     */
    protected static $uniqueProperties = [
        'getTitle',
        'getOwner',
    ];

    /**
     * @var string
     */
    protected static $baseUrl = 'images/projects';

    public function __construct()
    {
        $this->payments = new ArrayCollection();
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param string $title
     * @return $this
     */
    public function setTitle($title)
    {
        $this->title = $title;
        return $this;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param string $description
     * @return $this
     */
    public function setDescription($description)
    {
        $this->description = $description;
        return $this;
    }

    /**
     * @return float
     */
    public function getBudget()
    {
        return $this->budget;
    }

    /**
     * @param float $budget
     * @return $this
     */
    public function setBudget($budget)
    {
        $this->budget = $budget;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getReviewDate()
    {
        return clone $this->reviewDate;
    }

    /**
     * @param \DateTime|null $reviewDate
     * @return $this
     */
    public function setReviewDate(\DateTime $reviewDate = null)
    {
        $this->reviewDate = $reviewDate;
        return $this;
    }

    /**
     * @return int
     */
    public function getProjectTime()
    {
        return $this->projectTime;
    }

    /**
     * @param int $projectTime
     * @return $this
     */
    public function setProjectTime($projectTime)
    {
        $this->projectTime = $projectTime;
        return $this;
    }

    /**
     * @return int
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param int $status
     * @return $this
     */
    public function setStatus($status)
    {
        $this->status = $status;
        return $this;
    }

    /**
     * @return User
     */
    public function getOwner()
    {
        return $this->owner;
    }

    /**
     * @param User $owner
     * @return $this
     */
    public function setOwner(User $owner)
    {
        $this->owner = $owner;
        return $this;
    }

    /**
     * @return string
     */
    public function getDeclineDescription()
    {
        return $this->declineDescription;
    }

    /**
     * @param string|null $declineDescription
     * @return $this
     */
    public function setDeclineDescription($declineDescription = null)
    {
        $this->declineDescription = $declineDescription;
        return $this;
    }

    /**
     * @return UploadedFile
     */
    public function getImageFile()
    {
        return $this->imageFile;
    }

    /**
     * @param UploadedFile $imageFile
     * @return $this
     */
    public function setImageFile(UploadedFile $imageFile)
    {
        $this->imageFile = $imageFile;
        return $this;
    }

    /**
     * @return string
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * @param string $image
     * @return $this
     */
    public function setImage($image)
    {
        $this->image = $image;
        return $this;
    }

    /**
     * @ORM\PrePersist()
     */
    public function prePersist()
    {
        $this->saveImage();
    }

    /**
     * @param Investment $payment
     * @return Project
     */
    public function addPayment(Investment $payment)
    {
        $this->payments[] = $payment;
        $payment->setProject($this);
        return $this;
    }

    /**
     * @param Investment $payment
     * @return $this
     */
    public function removePayment(Investment $payment)
    {
        $this->payments->removeElement($payment);
        return $this;
    }

    /**
     * @return Collection
     */
    public function getPayments()
    {
        return $this->payments;
    }

    /**
     * @return float
     */
    public function getTotalFunds()
    {
        return array_sum($this->payments->map(function (Investment $investment) {
            return $investment->getAmount();
        })->toArray());
    }

    /**
     * @return int
     */
    public function getDaysLeft()
    {
        assert($this->isAccepted());
        $targetDate = $this->getReviewDate()->modify(sprintf('+ %ddays', $this->getProjectTime()))->getTimestamp();
        $result = $targetDate - time();
        if ($result < 0) {
            return 0;
        }
        return floor($result / 60 / 60 / 24);
    }

    /**
     * @return User[]
     */
    public function getInvestors()
    {
        return array_unique($this->payments->map(function (Investment $investment) {
            return $investment->getInvestor();
        })->toArray(), SORT_REGULAR);
    }

    /**
     * @return int
     */
    public function getProgress()
    {
        return round($this->getTotalFunds() / $this->budget * 100);
    }

    /**
     * @return bool
     */
    public function isAccepted()
    {
        return in_array($this->getStatus(), [
            ProjectStatus::ACCEPTED,
            ProjectStatus::FAILED,
            ProjectStatus::SUCCEED
        ]);
    }

    /**
     * @ORM\PostLoad()
     */
    public function reloadStatus()
    {
        if ($this->isAccepted() && $this->isExpired()) {
            $this->status = $this->getTotalFunds() < $this->getBudget() ? ProjectStatus::FAILED : ProjectStatus::SUCCEED;
        }
    }

    /**
     * @return bool
     */
    public function isExpired()
    {
        $now = new \DateTime();
        $endDate = $this->getReviewDate()->modify(sprintf('+ %s days', $this->getProjectTime()));
        return $now > $endDate;
    }
}
