<?php

namespace AppBundle\Entity;

use AppBundle\Exception\InsufficientFundsException;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Persistence\Proxy;
use FOS\UserBundle\Model\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Repository\UserRepository")
 * @ORM\Table(name="tbl_users")
 * @ORM\HasLifecycleCallbacks()
 */
class User extends BaseUser
{
    /**
     * @var float[] Percents in dependency of level
     */
    private static $percents = [
        0.25,
        0.2,
        0.15,
        0.15,
        0.15,
        0.1,
    ];

    /**
     * @ORM\Id()
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="float")
     */
    private $balance = 0;

    /**
     * @ORM\Column(name="account_cost", type="float")
     */
    private $accountCost = 0;

    /**
     * @ORM\Column(name="activation_date", type="datetime", nullable=true)
     */
    private $activationDate = null;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     * @Assert\Length(min="0", max="100", maxMessage="Имя не может превышать 100 символов")
     */
    private $name;

    /**
     * @ORM\Column(name="coins", type="float")
     */
    private $coins = 0;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $avatarPath;
    
    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\House", mappedBy="owner")
     * @var Collection $houses
     */
    private $houses;

    /**
     * @var UploadedFile
     * @Assert\Image()
     */
    private $avatarFile;

    /**
     * @ORM\Column(type="string", length=30, nullable=true)
     */
    private $skype;

    /**
     * @ORM\Column(name="success_views", type="smallint")
     */
    private $successViews = 0;

    /**
     * @ORM\Column(name="points", type="float")
     */
    private $points = 0;

    /**
     * @ORM\Column(name="fail_views", type="smallint")
     */
    private $failViews = 0;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\User")
     * @ORM\JoinColumn(referencedColumnName="id", name="referal", nullable=true)
     */
    private $referral;

    /**
     * @ORM\OneToOne(targetEntity="AppBundle\Entity\TrinarNode", mappedBy="user")
     * @ORM\JoinColumn(fieldName="trinar_node_id", referencedColumnName="id")
     * @var TrinarNode
     */
    private $trinar;

    /**
     * @var Collection
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Deposit", mappedBy="investor")
     */
    private $deposits;

    public function __construct()
    {
        parent::__construct();
        $this->roles = ['ROLE_INACTIVE'];
	    $this->houses = new ArrayCollection();
        $this->deposits = new ArrayCollection();
    }

    /**
     * @return \float[]
     */
    public static function getPercents()
    {
        return self::$percents;
    }

    public function getVideoPoints()
    {
        $result = $this->getSuccessViews() - $this->getFailViews();
        if ($result < 0) {
            return 0;
        }
        return $result;
    }

    /**
     * @return string
     */
    public function getAvatarPath()
    {
        return $this->avatarPath;
    }

    public function getHouseTypeList()
    {
        return array_unique($this->houses->filter(function (House $house) {
            return $house->getDeal() === null;
        })->map(function (House $house) {
            return $house->getType();
        })->toArray(), SORT_REGULAR);
    }

    public function getAvailableHouses($accountingDeal = true)
    {
        $result = new \SplObjectStorage();
        $availableTypes = $this->getHouseTypeList();
        foreach ($availableTypes as $houseType) {
            $result->attach(
                $houseType,
                $this->houses->filter(function (House $house) use ($houseType, $accountingDeal) {
                    $filter = $house->getType() === $houseType;
                    if ($accountingDeal) {
                        $filter &= $house->getDeal() === null;
                    }
                    return $filter;
                })
            );
        }
        return $result;
    }

    /**
     * @param string $avatarPath
     */
    public function setAvatarPath($avatarPath)
    {
        $this->avatarPath = $avatarPath;
    }

    /**
     * Uploads and sets avatar from profile form
     */
    public function uploadAvatar()
    {
        if (null === $this->getAvatarFile()) {
            return;
        }

        $avatarExtension = $this->getAvatarFile()->guessExtension();
        if (!isset($avatarExtension)) {
            $avatarExtension = $this->getAvatarFile()->guessClientExtension();
        }
        if (!isset($avatarExtension)) {
            $avatarExtension = $this->getAvatarFile()->getExtension();
        }
        if (!isset($avatarExtension)) {
            $avatarExtension = $this->getAvatarFile()->getClientOriginalExtension();
        }

        $this->getAvatarFile()->move(
            $this->getUploadRootDir(),
            $this->getId() . '.' . $avatarExtension
        );
        $this->avatarPath = $this->getUploadDir() . $this->getId() . '.' . $avatarExtension;
        $this->avatarFile = null;
    }

    public function getHouseCountOfType(HouseType $type, $accountingDeal = true)
    {
        return count($this->getHouses()->filter(function (House $house) use ($type, $accountingDeal) {
            return $accountingDeal ?
                ($house->getType() === $type && $house->getDeal() === null) :
                ($house->getType() === $type);
        }));
    }

    public function getTypedHouseCount($accountingDeal = true)
    {
        $houses = $this->houses;
        if ($accountingDeal) {
            $houses = $houses->filter(function (House $house) {
                return $house->getDeal() === null;
            });
        }
        return array_map(function (HouseType $type) use ($accountingDeal) {
            return [$this->getHouseCountOfType($type, $accountingDeal), $type];
        }, array_unique($houses
            ->map(function (House $house) { return $house->getType(); })
            ->toArray(), SORT_REGULAR));
    }

    /**
     * Sets file.
     *
     * @param UploadedFile $file
     */
    public function setAvatarFile(UploadedFile $file = null)
    {
        $this->avatarFile = $file;
    }

    /**
     * Get file.
     *
     * @return UploadedFile
     */
    public function getAvatarFile()
    {
        return $this->avatarFile;
    }

    /**
     * Set balance
     *
     * @param float $balance
     * @return User
     */
    public function setBalance($balance)
    {
        $this->balance = $balance;

        return $this;
    }

    /**
     * Get balance
     *
     * @return float 
     */
    public function getBalance()
    {
        return $this->balance;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return User
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    public function addHouseOfType(HouseType $type)
    {
        $house = new House();
        $house->setOwner($this);
        $house->setType($type);
        return $house;
    }

    /**
     * Set skype
     *
     * @param string $skype
     * @return User
     */
    public function setSkype($skype)
    {
        $this->skype = $skype;

        return $this;
    }

    public function canReplenishAccountCost()
    {
        return (!$this->hasRole('ROLE_INACTIVE')) && $this->accountCost < 1000;
    }

    /**
     * Get skype
     *
     * @return string 
     */
    public function getSkype()
    {
        return $this->skype;
    }

    /**
     * Set referal
     *
     * @param User $referal
     * @return User
     */
    public function setReferral(User $referal = null)
    {
        $this->referral = $referal;
        return $this;
    }

    /**
     * Get referral
     *
     * @return User
     */
    public function getReferral()
    {
        return $this->referral;
    }

    /**
     * @return TrinarNode
     */
    public function getTrinar()
    {
        return $this->trinar;
    }

    /**
     * @param float $amount
     * @return User[] Users that needs persistence
     */
    public function addPayment($amount)
    {
        $this->addBalance($amount);
        $parent = $this->getTrinar()->getParent();
        $needsPersistence = [];
        foreach (static::$percents as $level => $levelPercent) {
            if (isset($parent)) {
                $user = $parent->getUser();
                if ($user instanceof Proxy) {
                    $user->__load();
                }
                $needsPersistence[] = $user;
                $user->addBalance(
                    $amount * $levelPercent * $user->getAccountMultiplier()
                );
                $parent = $parent->getParent();
            } else {
                break;
            }
        }
        return $needsPersistence;
    }

    /**
     * @param float $amount
     * @return $this
     */
    private function addBalance($amount)
    {
        $this->balance += $amount;
        return $this;
    }

    /**
     * @param TrinarNode $trinar
     * @return $this
     */
    public function setTrinar($trinar)
    {
        $this->trinar = $trinar;
        return $this;
    }

    /**
     * Set successViews
     *
     * @param integer $successViews
     * @return User
     */
    public function setSuccessViews($successViews)
    {
        $this->successViews = $successViews;

        return $this;
    }

    /**
     * @return $this
     */
    public function addSuccessView()
    {
        $this->points++;
        $this->successViews++;
        return $this;
    }

    /**
     * @return $this
     */
    public function addFailView()
    {
        $this->failViews++;
        return $this;
    }

    /**
     * Get successViews
     *
     * @return integer 
     */
    public function getSuccessViews()
    {
        return $this->successViews;
    }

    /**
     * Set failViews
     *
     * @param integer $failViews
     * @return User
     */
    public function setFailViews($failViews)
    {
        $this->failViews = $failViews;

        return $this;
    }

    /**
     * Get failViews
     *
     * @return integer 
     */
    public function getFailViews()
    {
        return $this->failViews;
    }

    /**
     * @return string
     */
    private function getUploadRootDir()
    {
        return __DIR__.'/../../../web/' . $this->getUploadDir();
    }

    /**
     * @return string
     */
    public function getUploadDir()
    {
        return 'uploads/avatars/';
    }

    /**
     * @param string $salt
     * @return $this
     */
    public function setSalt($salt)
    {
        $this->salt = $salt;
        return $this;
    }

    /**
     * @return float
     */
    public function getAccountCost()
    {
        return $this->accountCost;
    }

    /**
     * @param float $accountCost
     * @return $this
     */
    public function setAccountCost($accountCost)
    {
        $this->accountCost = $accountCost;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getActivationDate()
    {
        return $this->activationDate;
    }

    /**
     * @param \DateTime $activationDate
     * @return $this
     */
    public function setActivationDate(\DateTime $activationDate = null)
    {
        if (!isset($activationDate)) {
            $activationDate = new \DateTime();
        }
        $this->activationDate = $activationDate;
        return $this;
    }

    /**
     * @return float
     */
    public function getAccountMultiplier()
    {
        return $this->accountCost / 10;
    }

    /**
     * Add house
     *
     * @param House $house
     * @return User
     */
    public function addHouse(House $house)
    {
        $this->houses[] = $house;

        return $this;
    }

    /**
     * Remove house
     *
     * @param House $house
     */
    public function removeHouse(House $house)
    {
        $this->houses->removeElement($house);
    }

    /**
     * Get houses
     *
     * @return Collection
     */
    public function getHouses()
    {
        return $this->houses;
    }
    
    public function getHousesCount()
    {
        return $this->houses->count();
    }

    public function pay($price)
    {
        $this->balance -= $price;
        return $this;
    }

    /**
     * Set coins
     *
     * @param integer $coins
     * @return User
     */
    public function setCoins($coins)
    {
        $this->coins = $coins;

        return $this;
    }

    /**
     * Get coins
     *
     * @return integer 
     */
    public function getCoins()
    {
        return $this->coins;
    }

    public function buyItem(Item $item)
    {
        $this->payWithCoins($item->getPrice());
        return (new InventoryItem())
            ->setItem($item)
            ->setUser($this)
        ;
    }

    public function payWithCoins($price)
    {
        if ($this->coins < $price) {
            throw new InsufficientFundsException();
        }
        $this->coins -= $price;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set points
     *
     * @param integer $points
     * @return User
     */
    public function setPoints($points)
    {
        $this->points = $points;

        return $this;
    }

    /**
     * Get points
     *
     * @return integer 
     */
    public function getPoints()
    {
        return $this->points;
    }

    /**
     * @param Deposit $deposit
     * @return $this
     */
    public function addDeposit(Deposit $deposit)
    {
        $this->deposits[] = $deposit;
        return $this;
    }

    public function removeDeposit(Deposit $deposit)
    {
        $this->deposits->removeElement($deposit);
        return $this;
    }

    /**
     * @return bool
     */
    public function hasDeposit()
    {
        return !!$this->deposits->filter(function (Deposit $deposit) {
            return !$deposit->isPaid();
        })->count();
    }

    /**
     * @return Collection
     */
    public function getDeposits()
    {
        return $this->deposits;
    }
}
