<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class HouseType
 * @package AppBundle\Entity
 * @ORM\Entity()
 * @ORM\Table(name="tbl_house_type")
 */
class HouseType
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(name="id", type="integer")
     */
    private $id;

    /**
     * @ORM\Column(name="image_path", type="string", length=20)
     */
    private $imageName;

    /**
     * @ORM\Column(name="title", type="string", length=50)
     */
    private $title;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\House", mappedBy="type")
     * @var Collection $houses
     */
    private $houses;

    /**
     * @ORM\Column(name="price", type="float")
     */
    private $price;

    public function __construct()
    {
        $this->houses = new ArrayCollection();
    }

    /**
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getImageName()
    {
        return $this->imageName;
    }

    /**
     * @param string $imageName
     * @return $this
     */
    public function setImageName($imageName)
    {
        $this->imageName = $imageName;
        return $this;
    }

    /**
     * @return string
     */
    public static function getImageBasePath()
    {
        return dirname(dirname(dirname(__DIR__))) . '/web';
    }

    /**
     * @return string
     */
    public static function getImageBaseUrl()
    {
        return '/images/house_types/';
    }

    public function getHouseImageUrl()
    {
        return '/images/houses/' . $this->getImageName() . '.png';
    }

    /**
     * @return string
     */
    public function getImagePath()
    {
        return static::getImageBasePath() . $this->getImageUrl();
    }

    /**
     * @return string
     */
    public function getImageUrl()
    {
        return static::getImageBaseUrl() . $this->getImageName() . '.png';
    }

    /**
     * @return string
     */
    public function getImageHoverUrl()
    {
        return static::getImageBaseUrl() . $this->getImageName() . '_hover.png';
    }

    /**
     * @return string
     */
    public function getImageHoverPath()
    {
        return static::getImageBasePath() . $this->getImageHoverUrl();
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param string $title
     * @return $this
     */
    public function setTitle($title)
    {
        $this->title = $title;
        return $this;
    }

    /**
     * @return float
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * @param float $price
     * @return $this
     */
    public function setPrice($price)
    {
        $this->price = $price;
        return $this;
    }

    /**
     * @return Collection
     */
    public function getHouses()
    {
        return $this->houses;
    }

    /**
     * @param Collection $houses
     * @return $this
     */
    public function setHouses($houses)
    {
        $this->houses = $houses;
        return $this;
    }

    public function addHouse(House $house)
    {
        $this->houses->add($house);
        return $this;
    }

    public function removeHouse(House $house)
    {
        $this->houses->removeElement($house);
        return $this;
    }
}
