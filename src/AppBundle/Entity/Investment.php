<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Investment for project
 * @package AppBundle\Entity
 * @ORM\Entity()
 * @ORM\Table(name="tbl_investments")
 */
class Investment
{
    /**
     * @var int
     * @ORM\Id()
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var User
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\User")
     * @ORM\JoinColumn(name="investor_id", referencedColumnName="id", onDelete="SET NULL")
     */
    private $investor;

    /**
     * @var float
     * @ORM\Column(type="float")
     * @Assert\NotBlank()
     * @Assert\GreaterThan(value="0")
     */
    private $amount;

    /**
     * @var Project
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Project", inversedBy="payments")
     * @ORM\JoinColumn(fieldName="project_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $project;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return User
     */
    public function getInvestor()
    {
        return $this->investor;
    }

    /**
     * @param User $investor
     * @return $this
     */
    public function setInvestor(User $investor)
    {
        $this->investor = $investor;
        return $this;
    }

    /**
     * @return float
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * @param float $amount
     * @return $this
     */
    public function setAmount($amount)
    {
        $this->amount = $amount;
        return $this;
    }

    /**
     * @return Project
     */
    public function getProject()
    {
        return $this->project;
    }

    /**
     * @param Project $project
     * @return $this
     */
    public function setProject(Project $project)
    {
        $this->project = $project;
        return $this;
    }
}
