<?php

namespace AppBundle\Form\Listener;

use AppBundle\Entity\User;
use FOS\UserBundle\Event\GetResponseUserEvent;
use FOS\UserBundle\FOSUserEvents;
use FOS\UserBundle\Model\UserManagerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\Session\Session;

class RegistrationListener implements EventSubscriberInterface
{

    /**
     * @var UserManagerInterface
     */
    private $userManager;

    /**
     * @var Session
     */
    private $session;

    /**
     * @param UserManagerInterface $userManager
     * @param Session $session
     */
    public function __construct(UserManagerInterface $userManager, Session $session)
    {
        $this->userManager = $userManager;
        $this->session = $session;
    }

    /** @inheritdoc */
    public static function getSubscribedEvents()
    {
        return [
            FOSUserEvents::REGISTRATION_INITIALIZE => 'onRegisterStart',
        ];
    }

    /**
     * @param GetResponseUserEvent $event
     */
    public function onRegisterStart(GetResponseUserEvent $event)
    {
        /** @var User $user */
        $user = $event->getUser();
        /** @var User $referral */
        $referral = $this->userManager
            ->findUserByUsername($this->session->get('referral'));
        $user->setReferral($referral);
    }
}