<?php

namespace AppBundle\Form\Type;

use AppBundle\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Validator\Constraints\GreaterThan;
use Symfony\Component\Validator\Constraints\LessThanOrEqual;

class DepositFormType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        /** @var User $user */
        $user = $options['user'];
        $builder
            ->add('current', 'bs_static', [
                'data' => sprintf('Текущий: %.2f$', $user->getAccountCost()),
            ])
            ->add('amount', 'number', [
                'constraints' => [
                    new GreaterThan(['value' => 0, 'message' => 'Сумма пополнения должна быть больше нуля!']),
                    new LessThanOrEqual(['value' => 1000 - $user->getAccountCost(), 'message' => 'Итоговый депозит не может превышать 1000$!']),
                    new LessThanOrEqual([
                        'value' => $user->getBalance(),
                        'message' => 'Недостаточно средств!',
                    ]),
                ],
                'error_bubbling' => true,
                'label' => false,
                'attr' => [
                    'placeholder' => sprintf('до %.2f$', 1000 - $user->getAccountCost()),
                ]
            ])
            ->add('submit', 'submit', ['label' => 'Пополнить депозит'])
        ;
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setRequired(['user']);
    }

    /** @inheritdoc */
    public function getName()
    {
        return 'activation_form_type';
    }
}