<?php

namespace AppBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Validator\Constraints\LessThanOrEqual;
use Symfony\Component\Validator\Constraints\Range;

class ProfileEditFormType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', 'text', [
                'label' => false,
                'attr' => [
                    'placeholder' => 'ФИО'
                ]
            ])
            ->add('skype', 'text', [
                'label' => false,
                'required' => false,
                'attr' => [
                    'placeholder' => 'Skype'
                ]
            ])
        ;
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(['data_class' => 'AppBundle\Entity\User']);
    }

    /** @inheritdoc */
    public function getName()
    {
        return 'profile_edit_form_type';
    }
}