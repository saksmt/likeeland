<?php

namespace AppBundle\Form\Type;

use FOS\UserBundle\Model\UserManagerInterface;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Validator\Constraints\Callback;
use Symfony\Component\Validator\Constraints\GreaterThan;
use Symfony\Component\Validator\Constraints\LessThanOrEqual;
use Symfony\Component\Validator\Context\ExecutionContextInterface;

/**
 * Go to profile form type
 * @package AppBundle\Form\Type
 */
class GoToProfileFormType extends AbstractType
{
    /**
     * @var UserManagerInterface
     */
    private $um;

    public function __construct(UserManagerInterface $um)
    {
        $this->um = $um;
    }

    /** @inheritdoc */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('username', 'text', [
                'label' => 'Логин пользователя',
            ])
            ->add('submit', 'submit', ['label' => 'Перейти'])
        ;
    }

    /** @inheritdoc */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
    }

    /** @inheritdoc */
    public function getName()
    {
        return 'go_to_profile_form';
    }
}