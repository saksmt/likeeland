<?php

namespace AppBundle\Form\Type;

use AppBundle\Form\Transformer\LoginToUserTransformer;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

/**
 * Registration form
 * @package AppBundle\Form\Type
 */
class RegistrationFormType extends AbstractType
{
    /**
     * @var LoginToUserTransformer
     */
    private $transformer;

    /**
     * @param LoginToUserTransformer $transformer
     */
    public function __construct(LoginToUserTransformer $transformer)
    {
        $this->transformer = $transformer;
    }

    /** @inheritdoc */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', null, ['label' => 'ФИО:'])
            ->add(
                $builder->create('referral', 'text', [
                    'label' => 'Имя пригласившего пользователя:',
                    'read_only' => true
                ])
                ->addModelTransformer($this->transformer)
            )
            ->add('captcha', 'captcha', [
                'label' => 'Защитный код:',
                'reload' => true,
                'background_color' => [255, 255, 255],
                'as_url' => true,
            ])
        ;
    }

    /** @inheritdoc */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(['data_class' => 'AppBundle\Entity\User']);
    }

    /** @inheritdoc */
    public function getName()
    {
        return 'user_register_form_type';
    }

    /** @inheritdoc */
    public function getParent()
    {
        return 'fos_user_registration';
    }
}