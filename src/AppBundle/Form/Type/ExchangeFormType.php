<?php

namespace AppBundle\Form\Type;

use AppBundle\Util\Enum\ExchangeRate;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints\GreaterThan;

class ExchangeFormType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add(ExchangeRate::POINTS . '-' . ExchangeRate::COINS, 'number', [
                'label' => 'Баллы <i class="glyphicon glyphicon-arrow-right"></i> золотые монеты',
                'required' => false,
                'constraints' => [
                    new GreaterThan(['value' => 0])
                ]
            ])
            ->add(ExchangeRate::COINS . '-' . ExchangeRate::MONEY, 'number', [
                'label' => 'Золотые монеты <i class="glyphicon glyphicon-arrow-right"></i> $',
                'required' => false,
                'constraints' => [
                    new GreaterThan(['value' => 0])
                ]
            ])
            ->add(ExchangeRate::MONEY . '-' . ExchangeRate::COINS, 'number', [
                'label' => '$ <i class="glyphicon glyphicon-arrow-right"></i> золотые монеты',
                'required' => false,
                'constraints' => [
                    new GreaterThan(['value' => 0])
                ]
            ])
            ->add('submit', 'submit', [
                'label' => 'Конвертировать'
            ])
        ;
    }

    /** @inheritdoc */
    public function getName()
    {
        return 'exchange_form_type';
    }
}