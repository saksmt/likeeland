<?php

namespace AppBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Validator\Constraints\LessThanOrEqual;

class ContributionFormType extends AbstractType
{

    /** @inheritdoc */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('amount', 'number', [
                'label' => 'Сумма вклада:',
                'attr' => ['placeholder' => sprintf('от 1.00$ до %.2f$', $options['balance'])],
                'constraints' => [
                    new LessThanOrEqual(['value' => $options['balance']]),
                ],
            ])
            ->add('submit', 'submit', [
                'label' => 'инвестировать'
            ])
        ;
    }

    /** @inheritdoc */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver
            ->setRequired(['balance'])
            ->setAllowedTypes('balance', ['float', 'int'])
            ->setDefaults(['data_class' => 'AppBundle\Entity\Deposit'])
        ;
    }

    /** @inheritdoc */
    public function getName()
    {
        return 'contribution_type';
    }
}