<?php

namespace AppBundle\Form\Type;

use AppBundle\Entity\User;
use AppBundle\Form\Transformer\HouseTypeToHouseTransformer;
use AppBundle\Validator\Constraints\AvailableHouseQuantity;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Validator\Constraints\GreaterThan;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\NotNull;

class SellHousesFormType extends AbstractType
{

    /**
     * @var User $user
     */
    private $user;
    private $ht;

    public function __construct(TokenStorageInterface $token, HouseTypeToHouseTransformer $ht)
    {
        $this->user = $token->getToken()->getUser();
        $this->ht = $ht;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add($builder->create('houses', 'bootstrap_collection', [
                'attr' => ['style' => 'inline'],
                'label' => 'Предметы сделки',
                'type' => 'house_form_type',
                'allow_add' => true,
                'allow_delete' => true,
                'delete_empty' => true,
                'add_button_text' => 'Добавить',
                'delete_button_text' => 'Удалить',
                'constraints' => [
                    new NotBlank(),
                    new AvailableHouseQuantity(),
                ],
            ])->addModelTransformer($this->ht))
            ->add('price', 'number', [
                'label' => 'Цена',
                'constraints' => [
                    new GreaterThan(['value' => 0]),
                ],
            ])
            ->add('submit', 'submit', [
                'label' => 'Выставить на продажу'
            ])
        ;
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults([
            'data_class' => 'AppBundle\Entity\Deal'
        ]);
    }

    /**
     * Returns the name of this type.
     *
     * @return string The name of this type
     */
    public function getName()
    {
        return 'sell_houses_form_type';
    }
}