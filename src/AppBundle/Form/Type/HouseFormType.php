<?php

namespace AppBundle\Form\Type;

use AppBundle\Entity\User;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Validator\Constraints\GreaterThan;

class HouseFormType extends AbstractType
{

    /**
     * @var User $user
     */
    private $user;

    public function __construct(TokenStorageInterface $tokenStorage)
    {
        $this->user = $tokenStorage->getToken()->getUser();
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('house_type', 'entity', [
                'label' => 'Тип владения',
                'class' => 'AppBundle\Entity\HouseType',
                'property' => 'title',
                'query_builder' => function (EntityRepository $repository) {
                    return $repository->createQueryBuilder('t')
                        ->select('t')
                        ->where('t IN(:available)')
                        ->setParameter('available', $this->user->getHouseTypeList())
                    ;
                },
                'expanded' => false,
                'multiple' => false,
            ])
            ->add('quantity', 'number', [
                'label' => 'Количество',
                'rounding_mode' => \NumberFormatter::ROUND_CEILING,
                'constraints' => [
                    new GreaterThan(['value' => 0]),
                ],
            ])
        ;
    }

    /**
     * Returns the name of this type.
     *
     * @return string The name of this type
     */
    public function getName()
    {
        return 'house_form_type';
    }
}