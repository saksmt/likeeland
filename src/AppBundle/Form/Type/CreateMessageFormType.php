<?php

namespace AppBundle\Form\Type;

use AppBundle\Form\Transformer\LoginToUserTransformer;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class CreateMessageFormType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('content', 'textarea', [
                'label' => 'Текст сообщения'
            ])
            ->add('submit', 'submit', [
                'label' => 'Отправить сообщение'
            ])
        ;
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults([
            'data_class' => 'AppBundle\Entity\Message'
        ]);
    }

    public function getName()
    {
        return 'create_message_form_type';
    }
}