<?php

namespace AppBundle\Form\Type;

use FOS\UserBundle\Model\UserManagerInterface;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Validator\Constraints\Callback;
use Symfony\Component\Validator\Constraints\GreaterThan;
use Symfony\Component\Validator\Constraints\LessThanOrEqual;
use Symfony\Component\Validator\Context\ExecutionContextInterface;

/**
 * Send money form
 * @package AppBundle\Form\Type
 */
class SendMoneyFormType extends AbstractType
{
    /**
     * @var UserManagerInterface
     */
    private $um;

    public function __construct(UserManagerInterface $um)
    {
        $this->um = $um;
    }

    /** @inheritdoc */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('amount', 'number', [
                'label' => 'Сумма',
                'constraints' => [new LessThanOrEqual([
                    'value' => $options['balance'],
                    'message' => 'Недостаточно средств!',
                ]), new GreaterThan(['value' => 0])],
            ])
            ->add('username', 'text', [
                'label' => 'Имя пользователя',
                'constraints' => [
                    new Callback(['callback' => function ($username, ExecutionContextInterface $context) use ($builder) {
                        if ($this->um->findUserByUsername($username) === null) {
                            $context->buildViolation('Пользователь "' . $username . '" не существует!');
                        }
                    }])
                ]
            ])
            ->add('form_submit', 'submit', ['label' => 'Отправить заявку'])
        ;
    }

    /** @inheritdoc */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver
            ->setRequired(['balance'])
        ;
    }

    /** @inheritdoc */
    public function getName()
    {
        return 'send_money_form';
    }
}