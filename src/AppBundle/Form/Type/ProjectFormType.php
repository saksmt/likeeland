<?php

namespace AppBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class ProjectFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title', null, ['label' => 'Название:'])
            ->add('description', 'textarea', ['label' => 'Описание:'])
            ->add('projectTime', 'number', ['label' => 'Срок(в днях):'])
            ->add('budget', 'number', ['label' => 'Бюджет:'])
            ->add('image_file', 'file', ['label' => 'Изображение:'])
            ->add('submit', 'submit', ['label' => 'Создать'])
        ;
    }

    /** @inheritdoc */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults([
            'data_class' => 'AppBundle\Entity\Project',
            'validation_groups' => ['create'],
        ]);
    }

    /** @inheritdoc */
    public function getName()
    {
        return 'project_type';
    }
}