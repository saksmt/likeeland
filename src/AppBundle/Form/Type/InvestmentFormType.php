<?php

namespace AppBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Validator\Constraints\LessThanOrEqual;

class InvestmentFormType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('amount', 'number', [
                'label' => 'Сумма инвестиции в бизнес идею',
                'constraints' => [
                    new LessThanOrEqual(['value' => $options['balance']])
                ]
            ])
            ->add('submit', 'submit', [
                'label' => 'Инвестировать'
            ])
        ;
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver
            ->setDefaults([
                'data_class' => 'AppBundle\Entity\Investment'
            ])
            ->setRequired(['balance'])
        ;
    }

    /**
     * @inheritdoc
     */
    public function getName()
    {
        return 'investment_form';
    }
}