<?php

namespace AppBundle\Form\Type;

use AppBundle\Entity\User;
use AppBundle\Util\Enum\ExchangeRate;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Validator\Constraints\GreaterThan;
use Symfony\Component\Validator\Constraints\LessThanOrEqual;

class SetExchangeRatesFormType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add(ExchangeRate::POINTS . '-' . ExchangeRate::COINS, 'number', [
                'label' => 'Количество золотых монет за один балл',
            ])
            ->add(ExchangeRate::COINS . '-' . ExchangeRate::MONEY, 'number', [
                'label' => 'Количество $ за одну золотую монету',
            ])
            ->add(ExchangeRate::MONEY . '-' . ExchangeRate::COINS, 'number', [
                'label' => 'Количество золотых монет за один $',
            ])
            ->add('submit', 'submit', [
                'label' => 'Сохранить настройки'
            ])
        ;
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
    }

    /** @inheritdoc */
    public function getName()
    {
        return 'set_exchange_rates_form_type';
    }
}