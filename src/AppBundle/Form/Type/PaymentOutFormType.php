<?php

namespace AppBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Validator\Constraints\GreaterThan;
use Symfony\Component\Validator\Constraints\LessThanOrEqual;

/**
 * Payment form
 * @package AppBundle\Form\Type
 */
class PaymentOutFormType extends AbstractType
{
    /** @inheritdoc */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->getData()->getUser()->getBalance();
        $builder
            ->add('amount', 'number', [
                'label' => 'Сумма',
                'constraints' => [new LessThanOrEqual([
                    'value' => $options['balance'],
                    'message' => 'Недостаточно средств!',
                ]), new GreaterThan(['value' => 0])],
            ])
            ->add('bankAccount', 'text', ['label' => 'Кошелек'])
            ->add('form_submit', 'submit', ['label' => 'Отправить заявку'])
        ;
    }

    /** @inheritdoc */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver
            ->setDefaults(['data_class' => 'AppBundle\Entity\Payment'])
            ->setRequired(['balance'])
        ;
    }

    /** @inheritdoc */
    public function getName()
    {
        return "payment_out_form";
    }
}