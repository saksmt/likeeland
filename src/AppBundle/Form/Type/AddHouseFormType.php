<?php

namespace AppBundle\Form\Type;

use AppBundle\Form\Transformer\LoginToUserTransformer;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class AddHouseFormType extends AbstractType
{

    public function __construct(LoginToUserTransformer $lut)
    {
        $this->loginToUserTransformer = $lut;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add($builder->create('user', 'text', ['label' => 'Имя пользователя'])->addModelTransformer($this->loginToUserTransformer))
            ->add('type', 'entity', [
                'label' => 'Тип дома',
                'class' => 'AppBundle\Entity\HouseType',
                'property' => 'title',
                'expanded' => false,
                'multiple' => false,
            ])
            ->add('quantity', 'number', ['label' => 'Количество домов'])
            ->add('submit', 'submit', ['label' => 'Добавить'])
            ->addEventListener(FormEvents::SUBMIT, [$this, 'onSubmit'])
        ;
    }

    public function onSubmit(FormEvent $event)
    {
        $data = $event->getData();
        $event->getForm()->add('persistenceList', 'hidden', []);
        $persistenceList = [];
        for ($i = 0; $i < $data['quantity']; $i++) {
            $persistenceList[] = $data['user']->addHouseOfType($data['type']);
        }
        $persistenceList[] = $data['user'];
        $event->getForm()->get('persistenceList')->setData($persistenceList);
    }

    public function getName()
    {
        return 'addhouse_form_type';
    }
}