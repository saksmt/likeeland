<?php

namespace AppBundle\Form\Transformer;

use AppBundle\Entity\SpecialCase\NonExistentHouse;
use AppBundle\Entity\User;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\PersistentCollection;
use Symfony\Component\Form\DataTransformerInterface;
use Symfony\Component\Form\Exception\TransformationFailedException;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

class HouseTypeToHouseTransformer implements DataTransformerInterface
{

    /**
     * @var User $user
     */
    private $user;

    public function __construct(TokenStorageInterface $token)
    {
        $this->user = $token->getToken()->getUser();
    }

    /**
     * @inheritdoc
     */
    public function transform($value)
    {
        $result = [];
        if (!isset($value)) {
            return $result;
        }
        if ($value instanceof PersistentCollection) {
            $value->initialize();
        }
        $hashMap = new \SplObjectStorage();
        foreach ($value as $house) {
            if (!$hashMap->offsetExists($house->getType())) {
                $hashMap->attach($house->getType(), 1);
            } else {
                $hashMap[$house->getType]++;
            }
        }
        foreach ($hashMap as $type => $quantity) {
            $result[] = ['house_type' => $type, 'quantity' => $quantity];
        }
        return $result;
    }

    /**
     * @inheritdoc
     */
    public function reverseTransform($value)
    {
        if (empty($value)) {
            return null;
        }
        $result = new ArrayCollection();
        $available = $this->user->getAvailableHouses();
        foreach ($value as $packedData) {
            $houseType = $packedData['house_type'];
            for ($i = 0; $i < $packedData['quantity']; $i++) {
                $found = $available[$houseType]->last();
                if ($found === false) {
                    $result[] = (new NonExistentHouse())->setType($houseType);
                } else {
                    $available[$houseType]->removeElement($found);
                    $result[] = $found;
                }
            }
        }
        return $result;
    }
}