<?php

namespace AppBundle\Form\Transformer;

use AppBundle\Manager\TrinarManager;
use FOS\UserBundle\Model\UserManagerInterface;
use Symfony\Component\Form\DataTransformerInterface;
use Symfony\Component\Form\Exception\TransformationFailedException;

/**
 * Transformer to convert username to user object
 * @package AppBundle\Form\Transformer
 */
class LoginToUserTransformer implements DataTransformerInterface
{

    /**
     * @var UserManagerInterface
     */
    private $manager;

    public function __construct(UserManagerInterface $userManager)
    {
        $this->manager = $userManager;
    }

    /** @inheritdoc */
    public function transform($user)
    {
        if (!isset($user)) {
            return null;
        }
        return $user->getUsername();
    }

    /** @inheritdoc */
    public function reverseTransform($login)
    {
        if (!isset($login) || $login === '') {
            return null;
        }
        $user = $this->manager->findUserByUsername($login);
        if (isset($user)) {
            return $user;
        } else {
            throw new TransformationFailedException(sprintf('No user found with "%s" username', $login));
        }
    }
}