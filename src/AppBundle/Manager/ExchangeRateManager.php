<?php

namespace AppBundle\Manager;

use AppBundle\Entity\User;
use AppBundle\Exception\InsufficientFundsException;
use BadMethodCallException;
use Symfony\Component\Config\Exception\FileLoaderLoadException;
use Symfony\Component\HttpKernel\Config\FileLocator;
use Symfony\Component\HttpKernel\KernelInterface;
use Symfony\Component\PropertyAccess\PropertyAccessor;

class ExchangeRateManager
{
    /**
     * @var User $user
     */
    private $user;

    /**
     * Amount value to exchange
     * @var int $amount
     */
    private $amount;

    /**
     * Exchange source option
     * @var string $source
     */
    private $source;

    /**
     * Exchange target option
     * @var string $target
     */
    private $target;

    /**
     * @var array Exchange rates
     */
    private $rates;

    /**
     * @var string Exchange rates file path
     */
    private $ratesPath;

    /**
     * @var bool Exchange rates update flag
     */
    private $updated = false;

    /**
     * @throws FileLoaderLoadException
     */
    public function __construct(KernelInterface $kernel)
    {
        $locator = new FileLocator($kernel);
        $this->ratesPath = $locator->locate('@AppBundle/Resources/data/exchange.json');
        $this->rates = json_decode(file_get_contents($this->ratesPath), true);
        if (!$this->rates) {
            throw new FileLoaderLoadException('Failed to load exchange rates!');
        }

    }

    public function setUser(User $user)
    {
        $this->user = $user;
        return $this;
    }

    public function setAmount($amount)
    {
        $this->amount = $amount;
        return $this;
    }

    public function setSource($type)
    {
        $this->source = $type;
        return $this;
    }

    public function setTarget($type)
    {
        $this->target = $type;
        return $this;
    }

    public function exchange()
    {
        if (!isset($this->user, $this->amount, $this->source, $this->target)) {
            throw new BadMethodCallException('Not enough parameters for exchange!');
        }
        $rate = $this->rates[$this->source][$this->target];
        $accessor = new PropertyAccessor();
        $sourceAmount = $accessor->getValue($this->user, $this->source);
        if ($sourceAmount < $this->amount) {
            throw new InsufficientFundsException();
        }
        $result = $accessor->getValue($this->user, $this->target) + $this->amount * $rate;
        $accessor->setValue($this->user, $this->target, $result);
        $accessor->setValue($this->user, $this->source, $sourceAmount - $this->amount);
        return $this->user;
    }

    public function getExchangeRates()
    {
        return $this->rates;
    }

    public function getRate($source, $target)
    {
        return $this->rates[$source][$target];
    }

    /**
     * @param $rates Array from SetExchangeRatesFormType
     * @return $this
     */
    public function setExchangeRates($rates)
    {
        foreach($rates as $path => $rate) {
            $paths = explode('-', $path);
            $this->updateExchangeRate($paths[0], $paths[1], $rate);
        }
        return $this;
    }

    public function updateExchangeRate($source, $target, $value)
    {
        $this->rates[$source][$target] = $value;
        $this->updated = true;
    }

    public function __destruct()
    {
        if ($this->updated) {
            file_put_contents($this->ratesPath, json_encode($this->rates));
        }
    }
}