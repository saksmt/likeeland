<?php

namespace AppBundle\Manager;

use AppBundle\Entity\TrinarNode;
use AppBundle\Entity\User;
use FOS\UserBundle\Model\UserManagerInterface;

/**
 * @package AppBundle\Util
 * Manages trinars
 */
class TrinarManager
{
    /**
     * @var UserManagerInterface
     */
    private $manager;

    /**
     * @var array
     */
    private $queue;
    private $rendered;
    private $debug = false;
    private $rootNodeName;

    /**
     * @param UserManagerInterface $manager
     * @param string $rootNodeName
     */
    public function __construct(UserManagerInterface $manager, $rootNodeName)
    {
        $this->manager = $manager;
        $this->rootNodeName = $rootNodeName;
    }

    /**
     * Get user queued for referring
     * @return User|null
     */
    public function getQueuedReferral()
    {
        foreach ($this->queue as $queued) {
            $user = $this->manager->findUserByUsername($queued);
            /** @var User $user */
            if (isset($user) && !$user->getTrinar()->isFull()) {
                return $user;
            }
        }
        return null;
    }

    /**
     * Registers user to trinar
     * @param User $user
     * @return User[] List of users, that needs persistence
     */
    public function registerReferral(User $user)
    {
        return $this->getRootNode()->addChild($user->getTrinar());
    }

    /**
     * @param TrinarNode $trinar
     * @return array
     */
    public function renderTrinar(TrinarNode $trinar, $level = 0)
    {
        if ($level === 0) {
            $this->rendered = [];
        }
        if ($this->debug) {
            var_dump(count($trinar->getChildren()));
        }
        if ($level > TrinarNode::NODE_CHILD_LEVELS_MAX) {
            return [];
        }
        foreach ($trinar->getChildren() as $node) {
            if ($this->debug) {
                echo '--------' . PHP_EOL;
            }
            $this->setRendered($level, $node->getUser()->getUsername());
            $this->renderTrinar($node, $level + 1);
            if ($this->debug) {
                echo '--------' . PHP_EOL;
            }
        }
        return $this->rendered;
    }

    public function setDebug()
    {
        $this->debug = true;
        return $this;
    }

    private function setRendered($level, $data)
    {
        if (!isset($this->rendered[$level])) {
            $this->rendered[$level] = [];
        }
        $this->rendered[$level][] = $data;
    }

    /**
     * @return TrinarNode
     */
    private function getRootNode()
    {
        return $this->manager->findUserByUsername($this->rootNodeName)->getTrinar();
    }

}