<?php

namespace AppBundle\Manager;

use AppBundle\Entity\Video;
use AppBundle\Util\Enum\VideoQuality;
use Buzz\Browser;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Bridge\Monolog\Logger;
use Symfony\Component\DomCrawler\Crawler;

class VideoManager
{
    /**
     * @var Browser
     */
    private $browser;

    /**
     * @var string
     */
    private $baseLink;

    /**
     * @var int
     */
    private $maxQuality;

    /**
     * @var ObjectManager
     */
    private $om;

    /**
     * @var Logger
     */
    private $logger;

    public function __construct(Browser $browser, ObjectManager $om, Logger $logger)
    {
        $this->browser = $browser;
        $this->om = $om;
        $this->logger = $logger;
    }

    /**
     * Create new video instance based on "share code"
     * @param $shareFrame
     * @return Video
     */
    public function create($shareFrame)
    {
        $this->baseLink = null;
        $this->maxQuality = -1;
        $url = $this->getFrameUrl($shareFrame);
        $this->logger->addInfo('Frame URL: ' . $url);
        $this->parseVideoData($this->getVideoPlayer($url));
        $video = new Video();
        $video
            ->setBaseLink($this->getBaseLink())
            ->setMaxQuality($this->getMaxQuality())
        ;
        $this->logger->info('Successfully parsed video!', [$video]);
        return $video;
    }

    private function getFrameUrl($shareFrame)
    {
        $url = null;
        preg_match('/src\="(.*?)"/', $shareFrame, $url);
        return 'http://' . substr($url[1], 2);
    }

    private function getVideoPlayer($url)
    {
        $document = $this->browser->get($url);
        return $document->getContent();
    }

    private function parseVideoData($videoPlayerDoc)
    {
        $crawler = new Crawler($videoPlayerDoc);
        $script = $crawler->filter('.progress + script')->getNode(0)->textContent;
        $data = null;
        preg_match('/var\s*vars\s*\=\s*(.*)/', $script, $data);
        $this->logger->addInfo('Raw VK "vars"', [$data]);
        $videoVars = json_decode($data[1], true);
        $this->logger->addInfo('Parsed VK "vars"', [$videoVars]);
        $qualityEnum = VideoQuality::class . '::Q';
        foreach ($videoVars as $key => $var) {
            $quality = null;
            if (!preg_match('/url([0-9]+)/', $key, $quality)) {
                continue;
            }
            $quality = $quality[1];
            if (!isset($this->baseLink)) {
                $this->createUrl($quality, $var);
//                $this->baseLink = str_replace([$quality, 'v6'], '%s', $var);
            }
            $qualityConstant = constant($qualityEnum . $quality);
            if ($qualityConstant > $this->maxQuality) {
                $this->maxQuality = $qualityConstant;
            }
        }

    }

    private function getBaseLink()
    {
        return $this->baseLink;
    }

    private function getMaxQuality()
    {
        return $this->maxQuality;
    }

    /**
     * Resets current video
     * @return array|null
     */
    public function resetCurrentVideo()
    {
        try {
            $currentVideo = $this->getCurrentVideo();
            if (isset($currentVideo)) {
                return $currentVideo->setCurrent(false);
            }
        } catch (\Exception $_) {
        } finally {
            return null;
        }
    }

    public function getCurrentVideo()
    {
        return $this->om->getRepository('AppBundle:Video')
            ->findOneBy(['current' => true]);
    }

    public function getLinks(Video $video)
    {
        $links = [];
        $maxQuality = $video->getMaxQuality();
        $baseLink = $video->getBaseLink();
        $definedQualities = VideoQuality::getAll();
        foreach ($definedQualities as $qualityName => $qualityConstant) {
            if ($qualityConstant > $maxQuality) {
                break;
            }
            $links[] = sprintf($baseLink, substr($qualityName, 1));
        }
        return $links;
    }

    private function createUrl($quality, $url)
    {
        $matches = null;
        preg_match('/^http\:\/\/cs[0-9]+(v[0-9])\./', $url, $matches);
        $url = str_replace($quality, '%s', $url);
        if (isset($matches[1])) {
            $position = strpos($url, $matches[1]);
            $url = substr_replace($url, '', $position, 2);
        }
        $this->baseLink = $url;
    }

}