<?php

use AppBundle\Util\Enum\ProjectStatus;

return [
    'status' => [
        ProjectStatus::DECLINED    => 'Отклонен',
        ProjectStatus::ACCEPTED    => 'Принят',
        ProjectStatus::SUCCEED     => 'Успешен',
        ProjectStatus::PENDING     => 'В обработке',
        ProjectStatus::FAILED      => 'Провален',
    ],
];
