<?php

namespace AppBundle\Twig;

use AppBundle\Entity\House;

class AppExtension extends \Twig_Extension
{

    public function getFilters()
    {
        return [
            new \Twig_SimpleFilter('houseTypes', [$this, 'countHouseTypes'], [
                'needs_environment' => true,
                'is_safe' => ['html'],
            ]),
        ];
    }

    /**
     * @param \Twig_Environment $env
     * @param House[] $houses
     * @param string $render
     * @return string
     */
    public function countHouseTypes(\Twig_Environment $env, $houses, $render = '<b>%s</b> %s шт.')
    {
        $preResult = [];
        foreach ($houses as $house) {
            $title = $house->getType()->getTitle();
            if (!isset($preResult[$title])) {
                $preResult[$title] = 0;
            }
            $preResult[$title] += 1;
        }
        $result = [];
        foreach ($preResult as $title => $count) {
            $result[] = sprintf($render, $title, $count);
        }
        return implode(', ', $result);
    }

    public function getName()
    {
        return 'app_extension';
    }
}