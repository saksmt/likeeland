<?php

namespace AppBundle\Validator\Constraints;

use AppBundle\Entity\House;
use AppBundle\Entity\SpecialCase\NonExistentHouse;
use Doctrine\Common\Collections\Collection;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

/**
 * Class AvailableHouseQuantityValidator
 * @package AppBundle\Validator\Constraints
 */
class AvailableHouseQuantityValidator extends ConstraintValidator
{

    /**
     * @inheritdoc
     */
    public function validate($value, Constraint $constraint)
    {
        if (!isset($value)) {
            return;
        }
        /** @var Collection $value */
        $nonExistent = $value->filter(function (House $house) {
            return $house instanceof NonExistentHouse;
        });
        if ($nonExistent->count()) {
            $invalidTypes = array_unique($nonExistent->map(function (House $house) {
                return $house->getType();
            })->toArray(), SORT_REGULAR);
            foreach ($invalidTypes as $type) {
                $this->context->addViolation($constraint->message, [
                    '%type%' => $type->getTitle(),
                ]);
            }
        }
    }
}