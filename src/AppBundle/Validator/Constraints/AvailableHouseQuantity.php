<?php

namespace AppBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;

/**
 * Class AvailableHouseQuantity
 * @package Validator\Constraints
 * @Annotation
 */
class AvailableHouseQuantity extends Constraint
{
    public $message = 'У вас не хватает резиденций типа "%type%" для продажи.';

    public function validatedBy()
    {
        return self::class . 'Validator';
    }
}