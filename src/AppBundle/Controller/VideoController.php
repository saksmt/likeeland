<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Video;
use AppBundle\Util\Enum\VideoQuality;
use Component\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class VideoController
 * @package AppBundle\Controller
 * @Route("/video0000")
 * @Security(expression="has_role('ROLE_ADMIN')")
 */
class VideoController extends Controller
{

    /**
     * @Route("/create", name="video_create")
     */
    public function createVideoAction(Request $request)
    {
        $video = (new Video())->setCurrent()->setUpdateTime(new \DateTime());
        $form = $this->createFormBuilder($video)
            ->add('baseLink', 'text', ['label' => 'Ссылка на видео:'])
            ->add('submit', 'submit', ['label' => 'Создать'])
            ->getForm();
        $form->handleRequest($request);
        if ($form->isValid()) {
            $vm = $this->get('video.manager');
            $needPersistence = [$video];
            $currentVideo = $vm->resetCurrentVideo();
            if (isset($currentVideo)) {
                $needPersistence[] = $currentVideo;
            }
            $this
                ->persistList($needPersistence)
                ->flush();
            $this->addFlash('success', 'Видео успешно добавлено!');
        }
        return $this->render('video/create.html.twig', ['form' => $form->createView()]);
    }

    /**
     * @Route("/video/success", name="videoProcessSuccess", defaults={"status" = true})
     * @Route("/video/fail", name="videoProcessFail", defaults={"status" = false})
     * @Security("has_role('ROLE_USER')")
     * @param bool $status
     * @param Request $request
     * @return RedirectResponse
     */
    public function processVideoAction($status, Request $request)
    {
        if ($request->cookies->get('system_settings')) {
            $expireTime = abs(round((time() - $request->cookies->get('system_settings')) / 60 / 60));
            $this->addFlash('alert', 'Вы уже смотрели фильм. Время до начала новой попытки: ' . $expireTime . ' часов!');
            return $this->redirectToRoute('profile');
        } else {
            $response = new RedirectResponse($this->generateUrl('profile'));
            $expireTime = time() + (60 * 60 * 6);
            $response->headers->setCookie(new Cookie('system_settings', $expireTime, $expireTime, '/'));
        }
        $user = $this->getUser();
        if ($status) {
            $user->addSuccessView();
            $this->addFlash('success', 'Просмотр засчитан!');
        } else {
            $this->addFlash('error', 'Просмотр не засчитан!');
            $user->addFailView();
        }
        $em = $this->getDoctrine()->getManager();
        $em->persist($user);
        $em->flush();

        return $response;
    }

    /**
     * @Route("/", name="video")
     * @Security("has_role('ROLE_USER')")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction(Request $request)
    {
        //$this->addFlash('alert', 'Видео скоро будет доступно. Ожидайте...');
        //return $this->redirectToRoute('profile');
        if ($request->cookies->get('system_settings')) {
            $expireTime = abs(round((time() - $request->cookies->get('system_settings')) / 60 / 60));
            $this->addFlash('alert', 'Вы уже смотрели фильм. Время до начала просмотра следующего фильма: ' . $expireTime . ' часов!');
            return $this->redirectToRoute('profile');
        }

        $vm = $this->get('video.manager');
        $videos = $vm->getLinks($vm->getCurrentVideo());
        return $this->render('video/index.html.twig', [
            'videoLinks' => $videos,
            'qualities' => array_keys(VideoQuality::getAll())
        ]);
    }
}