<?php

namespace AppBundle\Controller;

use AppBundle\Entity\HouseMessage;
use AppBundle\Entity\Message;
use AppBundle\Entity\Payment;
use AppBundle\Entity\UsersHouses;
use AppBundle\Entity\Video;
use AppBundle\Form\Type\CreateMessageFormType;
use AppBundle\Util\Enum\PaymentStatus;
use AppBundle\Util\Enum\PaymentType;
use DateTime;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Component\Controller\Controller;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * Administrator controller
 * @package AppBundle\Controller
 * @Route("/admin")
 * @Security("has_role('ROLE_ADMIN')")
 */
class AdminController extends Controller
{

    /**
     * @Route("/payments_out", name="paymentsOut")
     */
    public function paymentsOutAction()
    {
        $payments = $this->getDoctrine()->getRepository('AppBundle:Payment')->findWithUserBy([
            'type' => PaymentType::OUT,
            'status' => PaymentStatus::PENDING,
        ]);
        return $this->render('admin/paymentsOut.html.twig', ['payments' => $payments]);
    }

    /**
     * @Route("/create_message", name="createMessage")
     */
    public function createMessageAction(Request $request)
    {
        $message = new Message();
        $form = $this->createForm(new CreateMessageFormType(), $message);
        $form->handleRequest($request);
        if ($form->isValid()) {
            set_time_limit(0); // for apocalypse purposes
            $allHouses = $this->getDoctrine()->getRepository('AppBundle:House')->findAll();
            $persister = $this->get('persister.factory')->createPersister();
            $persister->persist($message);
            foreach($allHouses as $house) {
                $houseMessage = new HouseMessage();
                $houseMessage->setMessage($message);
                $houseMessage->setHouse($house);
                $persister->persist($houseMessage);
            }
            $persister->flush();
            $this->addFlash('success', 'Сообщения отправлены всем владельцам резиденций.');
        }
        return $this->render('admin/createMessage.html.twig', ['form' => $form->createView()]);
    }

    /**
     * @Route("/add_house", name="addHouse")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function addHouseAction(Request $request)
    {
        $form = $this->createForm('addhouse_form_type');
        $form->handleRequest($request);
        if ($form->isValid()) {
            $this->persistList($form->get('persistenceList')->getData())->flush();
            $this->addFlash('success', 'Дома добавлены пользователю');
            return $this->redirectToRoute('addHouse');
        }
        return $this->render('admin/addHouse.html.twig', ['form' => $form->createView()]);
    }

    /**
     * @Route("/video_views", name="videoViews")
     */
    public function videoViewsAction(Request $request)
    {
        /** @var Video $video */
        $video = $this->getDoctrine()->getRepository('AppBundle:Video')->findOneBy(['current' => true]);
        $form = $this->createFormBuilder($video)
            ->add('currentViews', 'number', ['label' => 'Текущие просмотры'])
            ->add('maxViews', 'number', ['label' => 'Всего просмотров'])
            ->add('save', 'submit', ['label' => 'Сохранить изменения'])
            ->getForm();
        $form->handleRequest($request);
        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $video->setUpdateTime(new DateTime());
            $em->persist($video);
            $em->flush();
            $this->addFlash('success', 'Изменения сохранены!');
            return $this->redirectToRoute('videoViews');
        }
        return $this->render('admin/videoViewEdit.html.twig', ['form' => $form->createView()]);
    }

    /**
     * @Route("/payments_out/{id}/accept", name="processOutAccept", defaults={"accept" = true})
     * @Route("/payments_out/{id}/decline", name="processOutDecline", defaults={"accept" = false})
     * @param Payment $payment
     * @param boolean $accept
     * @return RedirectResponse
     */
    public function processOutAction(Payment $payment, $accept)
    {
        $payment->setStatus(($accept) ? PaymentStatus::SUCCESS : PaymentStatus::FAIL);
        $persistenceList = [$payment];
        if (!$accept) {
            $user = $payment->getUser();
            $user->setBalance($user->getBalance() + $payment->getAmount());
            $persistenceList[] = $user;
        }
        $this
            ->persistList($persistenceList)
            ->flush();
        return $this->redirect($this->generateUrl('paymentsOut'));
    }

}
