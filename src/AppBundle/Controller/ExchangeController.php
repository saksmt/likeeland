<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Deal;
use AppBundle\Entity\User;
use AppBundle\Util\Enum\PaymentStatus;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Component\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * Exchange controller
 * @package AppBundle\Controller
 * @Route("/exchange")
 * @Security("has_role('ROLE_USER')")
 */
class ExchangeController extends Controller
{

    /**
     * @Route("/my_deals/{page}", name="myDeals", requirements={"page"="\d+"}, defaults={"page"=1})
     */
    public function myDealsAction($page)
    {
        $userDeals = $this->get('knp_paginator')->paginate(
            $this->getDoctrine()->getRepository('AppBundle:Deal')->queryActiveByUser($this->getUser()),
            $page
        );
        return $this->render('exchange/my_deals.html.twig', ['userDeals' => $userDeals]);
    }

    /**
     * @Route("/sell_houses", name="sellHouses")
     */
    public function sellHousesAction(Request $request)
    {
        $typedHouses = $this->getUser()->getTypedHouseCount();
        if (count($typedHouses) == 0) {
            $this->addFlash('danger', 'Вам нечего продавать!');
            return $this->redirectToRoute('profile');
        }
        $deal = new Deal();
        $deal->setSeller($this->getUser());
        $form = $this->createForm('sell_houses_form_type', $deal);
        $form->handleRequest($request);
        if ($form->isValid()) {
            $this->persistList([$deal])->flush();
            $this->addFlash('success', 'Резиденции успешно выставлены на биржу!');
            return $this->redirectToRoute('profile');
        }
        return $this->render('exchange/sell_houses.html.twig', ['form' => $form->createView(), 'typedHouses' => $typedHouses]);
    }

    /**
     * @Route("/list/{page}", name="listDeals", requirements={"page"="\d+"}, defaults={"page"=1})
     */
    public function listAction(Request $request, $page)
    {
        $houseTypeRepository = $this->getDoctrine()->getRepository('AppBundle:HouseType');
        $houseTypes = $houseTypeRepository->findAll();
        $type = $houseTypeRepository->find($request->get('type') % (count($houseTypes) + 1));
        $dealRepository = $this->getDoctrine()->getRepository('AppBundle:Deal');
        $deals = (!isset($type)) ? $dealRepository->queryAllActive() : $dealRepository->queryActiveByType($type);
        $deals = $this->get('knp_paginator')->paginate($deals, $page);
        return $this->render('exchange/list_houses.html.twig', ['houseTypes' => $houseTypes, 'deals' => $deals]);
    }

    /**
     * @Route("/remove/{id}", name="removeDeal", requirements={"id"="\d+"})
     * @param Deal $deal
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function removeDealAction(Deal $deal)
    {
        if ($deal->getSeller() === $this->getUser()) {
            if ($deal->getStatus() == PaymentStatus::PENDING) {
                $this->persistList(array_merge([$deal], $deal->dismiss()))->flush();
                $this->addFlash('success', 'Предложение удалено');
                return $this->redirectToRoute('myDeals');
            } else {
                throw $this->createAccessDeniedException();
            }
        } else {
            throw $this->createAccessDeniedException();
        }
    }

    /**
     * @Route("/buy/{id}", name="buyDeal", requirements={"id"="\d+"})
     * @param Deal $deal
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function buyDealAction(Deal $deal)
    {
        /** @var User $user */
        $user = $this->getUser();
        if ($deal->getStatus() == PaymentStatus::PENDING) {
            if ($user->getBalance() >= $deal->getPrice()) {
                $this->persistList($deal->buy($user))->flush();
                $this->addFlash('success', 'Поздравляем с покупкой!');
                return $this->redirectToRoute('purchases');
            } else {
                $this->addFlash('danger', 'У вас недостаточно средств для покупки!');
                return $this->redirectToRoute('profile');
            }
        } else {
            throw $this->createAccessDeniedException();
        }
    }

}
