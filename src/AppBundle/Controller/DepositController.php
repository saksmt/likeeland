<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Deposit;
use AppBundle\Entity\User;
use AppBundle\Form\Type\ContributionFormType;
use Component\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class DepositController
 * @package AppBundle\Controller
 * @Route("/deposit")
 * @Security(expression="has_role('ROLE_USER')")
 */
class DepositController extends Controller
{
    /**
     * @Route("/", name="depositList")
     * @return Response
     */
    public function indexAction()
    {
        $user = $this->getUser();
        if ($user->hasDeposit()) {
            $this->addFlash('danger', 'Дождитесь окончания срока действия вклада!');
            return $this->redirectToRoute('profile');
        }
        return $this->render('deposit/index.html.twig', [
            'quickDeposit' => Deposit::THREE_DAY_PERCENTAGE,
            'monthDeposit' => Deposit::MONTH_PERCENTAGE,
            'weekDeposit' => Deposit::WEEK_PERCENTAGE,
        ]);
    }

    /**
     * @param string $period
     * @Route("/invest/{period}", requirements={"period"="(3days)|(week)|(month)"}, name="createDeposit")
     * @return Response
     */
    public function investAction($period, Request $request)
    {
        $user = $this->getUser();
        if ($user->hasDeposit()) {
            $this->addFlash('danger', 'Дождитесь окончания срока действия вклада!');
            return $this->redirectToRoute('profile');
        }
        $period = strtolower($period);
        $deposit = null;
        if ($period === '3days') {
            $deposit = Deposit::createQuickDeposit($user);
        } elseif ($period === 'week') {
            $deposit = Deposit::createForWeek($user);
        } else {
            $deposit = Deposit::createForMonth($user);
        }
        $form = $this->createForm(
            new ContributionFormType(),
            $deposit,
            ['balance' => $user->getBalance()]
        );
        $form->handleRequest($request);
        if ($form->isValid()) {
            /** @var User $user */
            $this
                ->persistList([$deposit, $user->pay($deposit->getAmount())])
                ->flush();
            $this->addFlash('success', 'Вклад успешно создан!');
            return $this->redirectToRoute('profile');
        }
        return $this->render('deposit/invest.html.twig', ['form' => $form->createView()]);
    }
}