<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Project;
use AppBundle\Form\Type\DeclineProjectFormType;
use AppBundle\Form\Type\ProjectFormType;
use AppBundle\Entity\Investment;
use AppBundle\Entity\User;
use AppBundle\Form\Type\InvestmentFormType;
use AppBundle\Util\Enum\ProjectStatus;
use Component\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\Form\FormView;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class StartupController
 * @package AppBundle\Controller
 * @Route("/startup")
 * @Security(expression="has_role('ROLE_USER')")
 */
class StartupController extends Controller
{
    /**
     * @Route("/list/{page}", name="startupList", requirements={"page"="\d+"}, defaults={"page"=1})
     * @param int $page
     * @return Response
     */
    public function indexAction($page)
    {
        $projects = $this->getDoctrine()->getRepository('AppBundle:Project')
            ->queryAllAccepted();
        $elements = $this->get('knp_paginator')->paginate($projects, $page, 12);
        return $this->render('startup/list.html.twig', ['elements' => $elements]);
    }

    /**
     * @Route("/view/{id}", name="startupView", requirements={"id"="\d+"})
     * @param Project $project
     * @param Request $request
     * @return Response
     */
    public function viewAction(Project $project, Request $request)
    {
        /** @var User $user */
        $user = $this->getUser();
        if (!$project->isAccepted() && !$user->hasRole('ROLE_ADMIN')) {
            throw $this->createAccessDeniedException();
        }
        if ($project->getStatus() == ProjectStatus::DECLINED) {
            return $this->redirectToRoute('startupList');
        }
        $invest = new Investment();
        $form = $this->createForm(new InvestmentFormType(), $invest, [
            'balance' => $user->getBalance(),
        ]);
        $form->handleRequest($request);
        if ($form->isValid()) {
            if ($project->getStatus() != ProjectStatus::ACCEPTED) {
                throw $this->createAccessDeniedException();
            }
            $invest->setInvestor($user);
            $invest->setProject($project);
            $user->pay($invest->getAmount());
            $this->persistList([$user, $invest])->flush();
            $this->addFlash(
                'success',
                'Вы успешно инвестировали ' . $invest->getAmount() . '$ в бизнес идею!'
            );
            return $this->redirectToRoute('startupView', [
                'id' => $project->getId()
            ]);
        }
        $declineForm = $this->handleDeclineForm($project, $request);
        if ($project->getStatus() == ProjectStatus::DECLINED) {
            return $this->redirectToRoute('startupList');
        }
        $hasErrors = $form->getErrors(true)->count();
        return $this->render('startup/view.html.twig', [
            'project' => $project,
            'form' => $form->createView(),
            'declineForm' => $declineForm,
            'hasErrors' => $hasErrors,
        ]);
    }

    /**
     * @param Request $request
     * @return Response
     * @Route("/create", name="startupCreate")
     */
    public function createAction(Request $request)
    {
        $user = $this->getUser();
        $project = (new Project())->setOwner($user);
        $form = $this->createForm(new ProjectFormType(), $project);
        $form->handleRequest($request);
        if ($form->isValid()) {
            $this->persistList([$project, $user])->flush();
            $this->addFlash('success', 'Бизнес идея успешно создана!');
            return $this->redirectToRoute('startupCreate');
        }
        return $this->render('startup/create.html.twig', ['form' => $form->createView()]);
    }

    /**
     * @param Project $project
     * @return Response
     * @Route("/accept/{id}", requirements={"id"="\d+"}, name="startupAccept")
     * @Security(expression="has_role('ROLE_ADMIN')")
     */
    public function acceptAction(Project $project)
    {
        if ($project->isAccepted()) {
            throw $this->createAccessDeniedException();
        }
        $this->persistList([
            $project
                ->setStatus(ProjectStatus::ACCEPTED)
                ->setReviewDate(new \DateTime())
        ])->flush();
        return $this->redirectToRoute('startupView', ['id' => $project->getId()]);
    }

    /**
     * @param $page
     * @return Response
     * @Route("/admin/list/{page}", requirements={"page"="\d+"}, defaults={"page"=1}, name="startupAdminList")
     * @Security(expression="has_role('ROLE_ADMIN')")
     */
    public function adminListAction($page)
    {
        $projects = $this->get('knp_paginator')
            ->paginate(
                $this->getDoctrine()->getManager()->getRepository('AppBundle:Project')
                    ->findBy(['status' => ProjectStatus::PENDING]),
                $page,
                12
            );
        return $this->render('startup/pendingList.html.twig', ['projects' => $projects]);
    }

    /**
     * @param $page
     * @return Response
     * @Route("/history/{page}", name="startupHistory", requirements={"page"="\d+"}, defaults={"page"=1})
     */
    public function historyAction($page)
    {
        $projects = $this
            ->getDoctrine()
            ->getRepository('AppBundle:Project')
            ->findBy([
                'owner' => $this->getUser()
            ])
        ;
        $projects = $this->get('knp_paginator')->paginate($projects);
        return $this->render('startup/history.html.twig', ['projects' => $projects]);
    }

    /**
     * @param Project $project
     * @param Request $request
     * @return FormView
     */
    private function handleDeclineForm(Project $project, Request $request)
    {
        $form = $this->createForm(new DeclineProjectFormType(), $project);
        $form->handleRequest($request);
        $user = $this->getUser();
        if ($form->isValid()) {
            if (!$user->hasRole('ROLE_ADMIN')) {
                throw $this->createAccessDeniedException();
            }
            $project->setStatus(ProjectStatus::DECLINED);
            $this->persistList([$project])->flush();
            $this->addFlash('success', 'Успешно отклонена!');
        }
        return $form->createView();
    }
}