<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Payment;
use AppBundle\Entity\TrinarNode;
use AppBundle\Entity\User;
use AppBundle\Form\Type\DepositFormType;
use AppBundle\Form\Type\PaymentOutFormType;
use AppBundle\Form\Type\ProfileEditFormType;
use AppBundle\Util\Enum\PaymentStatus;
use AppBundle\Util\Enum\PaymentType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Component\Controller\Controller;
use Symfony\Component\Form\FormView;
use Symfony\Component\HttpFoundation\Cookie;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;

/**
 * Site's main controller
 * @package AppBundle\Controller
 */
class DefaultController extends Controller
{
    /**
     * @Route("/", name="homepage")
     * @Route("/ref/{referral}", name="homepage_ref")
     * @param string|null $referral
     * @return Response
     */
    public function indexAction($referral = null)
    {
        if ($this->get('security.context')->isGranted('ROLE_USER')) {
            return $this->redirectToRoute('profile');
        }
        $this->get('session')->set('referral', $referral);
        if (isset($referral)) {
            return $this->redirectToRoute('fos_user_registration_register');
        } else {
            return $this->render('default/index.html.twig');
        }
    }

    /**
     * @Route("/shares", name="shareView")
     * @Security(expression="has_role('ROLE_SHAREHOLDER')")
     */
    public function shareAction()
    {
        return $this->render('default/share.html.twig');
    }

    /**
     * @Route("/partners/{page}", name="userList", requirements={"page"="\d+"}, defaults={"page"=1})
     * @param int $page
     * @return Response
     */
    public function partnersAction($page, Request $request)
    {
        $gotoForm = $this->createForm('go_to_profile_form');
        $gotoForm->handleRequest($request);
        if ($gotoForm->isValid()) {
            $user = $gotoForm->getData()['username'];
            $user = $this->getDoctrine()->getRepository('AppBundle:User')->findOneBy(['username' => $user]);
            if (isset($user)) {
                return $this->redirectToRoute('profileViewGuest', ['id' => $user->getId()]);
            } else {
                $this->addFlash('danger', 'Пользователь с таким логином не найден!');
            }
        }
        $allUsers =
            $this
                ->getDoctrine()
                ->getRepository('AppBundle:User')
                ->findAllWithAvatar();
        $paginator = $this->get('knp_paginator')->paginate($allUsers, $page, 16);
        return $this->render('default/partners.html.twig', ['users' => $paginator, 'gotoForm' => $gotoForm->createView()]);
    }

    /**
     * @Route("/profile/view/{id}", name="profileViewGuest", requirements={"id"="\d+"})
     * @param User $user
     * @return Response
     */
    public function profileView(User $user)
    {
        if (!isset($user)) throw $this->createNotFoundException();
        return $this->render('default/profileGuestView.html.twig', ['user' => $user]);
    }

    /**
     * @param Request $request
     * @return FormView
     */
    private function processSendMoney(Request $request)
    {
        $user = $this->getUser();
        $sendMoneyForm = $this->get('form.factory')->create(
            'send_money_form',
            [],
            ['balance' => $user->getBalance()]
        );
        $sendMoneyForm->handleRequest($request);
        if ($sendMoneyForm->isValid()) {
            $data = $sendMoneyForm->getData();
            /** @var User $recipient */
            $recipient = $this->get('fos_user.user_manager')->findUserByUsername($data['username']);
            $inPayment = new Payment();
            $inPayment
                ->setAmount($data['amount'])
                ->setBankAccount('U:' . $user->getUsername())
                ->setStatus(PaymentStatus::SUCCESS)
            ;
            $outPayment = clone $inPayment;
            $inPayment
                ->setUser($recipient)
                ->setType(PaymentType::IN)
                ->setComment('Перевод средств от "' . $user->getUsername() . '"');
            $outPayment
                ->setUser($user)
                ->setType(PaymentType::OUT)
                ->setComment('Перевод средств пользователю "' . $recipient->getUsername() . '"');
            if (isset($recipient)) {
                $user->setBalance($user->getBalance() - $data['amount']);
                $recipient->setBalance($recipient->getBalance() + $data['amount']);
                $this
                    ->persistList([$user, $recipient, $inPayment, $outPayment])
                    ->flush();
                $this->addFlash('success', 'Деньги успешно переведены!');
            } else {
                $this->addFlash('error', 'Пользователя не существует!');
            }
        }
        return $sendMoneyForm->createView();
    }

    /**
     * @Route("/getRefers/{username}", name="getRefers")
     * @param string $username
     * @return JsonResponse
     */
    public function getReferalsAction($username)
    {
        $userObject = $this->get('fos_user.user_manager')->findUserByUsername($username);
        if (!isset($userObject)) {
            return new JsonResponse();
        }
        $referals = $this->getDoctrine()->getRepository('AppBundle:User')->findBy(['referral' => $userObject]);
        $referals = array_map(function (User $user) {
            return $user->getUsername();
        }, $referals);
        return new JsonResponse($referals);
    }

    /**
     * @Route("/profile", name="profile")
     * @Route("/profile/", name="profile_slash")
     * @Security(expression="has_role('ROLE_USER')")
     * @param Request $request
     * @return Response
     */
    public function profileAction(Request $request)
    {
        if (!$request->cookies->get('cleared')) {
            $response = new RedirectResponse($this->generateUrl('profile'));
            $expireTime = time() + (60 * 60 * 6);
            $response->headers->setCookie(new Cookie('cleared', true, $expireTime, '/'));
            $response->headers->setCookie(new Cookie('system_settings', 0, 0, '/'));
            $this->addFlash('success', 'Ваша видеосессия начата. Можете смотреть видео!');
            return $response;
        }
        $video = $this->getDoctrine()->getRepository('AppBundle:Video')->findOneBy(['current' => true]);
        return $this->render('default/profileView.html.twig', array_merge(
            $this->processPaymentForm($request),
            [
                'uploadAvatarForm'  => $this->processAvatarForm($request),
                'profileEditForm'   => $this->processProfileForm($request),
                'sendMoneyForm'     => $this->processSendMoney($request),
                'depositForm'       => $this->processDepositForm($request),
                'video'             => $video
            ]
        ));
    }

    /**
     * @param Request $request
     * @return FormView
     */
    private function processAvatarForm(Request $request)
    {
        $user = $this->getUser();
        $form = $this->createFormBuilder($user)->add('avatarFile', 'file')->getForm();
        $form->handleRequest($request);
        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $user->uploadAvatar();
            $em->persist($user);
            $em->flush();
            $this->addFlash('success', 'Изображение профиля изменено!');
        }
        return $form->createView();
    }

    /**
     * @param Request $request
     * @return FormView
     */
    private function processProfileForm(Request $request)
    {
        $user = $this->getUser();
        $form = $this->createForm(new ProfileEditFormType(), $user);
        $form->handleRequest($request);
        if ($form->isValid()) {
            $this
                ->persistList([$user])
                ->flush()
            ;
            $this->addFlash('success', 'Изменения сохранены');
        }
        return $form->createView();
    }

    /**
     * @Route("/trinar", name="trinar")
     * @Security("has_role('ROLE_USER')")
     */
    public function trinarAction()
    {
        $referrals = $this->getDoctrine()->getRepository('AppBundle:User')
            ->findBy(['referral' => $this->getUser()]);
        $trinar = $this->getUser()->getTrinar();
        if (!isset($trinar)) {
            $this->addFlash('alert', 'Необходима активация в тринаре!');
            return $this->redirectToRoute('profile');
        }
        return $this->render('default/trinarView.html.twig', [
            'referrals' => $referrals,
            'trinarView' => $this->get('trinar.manager')->renderTrinar(
                $trinar
            ),
        ]);
    }

    /**
     * @Route("/payment", name="payment")
     * @param Request $request
     * @return RedirectResponse
     */
    public function paymentAction(Request $request)
    {
        if ($this->get('merchant')->verify($request->request, function (
            User $user,
            $amount,
            $bankAccount
        ) {
            $persistenceList = [$user->setBalance($user->getBalance() + $amount)];
            $payment = new Payment();
            $payment
                ->setAmount($amount)
                ->setUser($user)
                ->setType(PaymentType::IN)
                ->setBankAccount($bankAccount)
                ->setStatus(PaymentStatus::SUCCESS)
                ->setComment('Пополнение кошелька')
            ;
            $persistenceList[] = $payment;
            $persistenceList[] = $user;
            $this
                ->persistList($persistenceList)
                ->flush();
        })) {
            $this->addFlash('success', 'Платёж успешно обработан!');
        } else {
            $this->addFlash('error', 'Платёж не выполнен!');
        }
        return $this->redirect($this->generateUrl('profile'));
    }

    private function processDepositForm(Request $request)
    {
        $user = $this->getUser();
        $form = $this->createForm(new DepositFormType(), [], [
            'user' => $user,
        ]);
        $form->handleRequest($request);
        if ($form->isValid()) {
            $user->setBalance($user->getBalance() - $form->getData()['amount']);
            $user->setAccountCost($user->getAccountCost() + $form->getData()['amount']);
            $this->persistList([$user])->flush();
            $this->addFlash('success', 'Депозит пополнен!');
        }
        $errors = $form->getErrors();
        foreach ($errors as $error) {
            $this->addFlash('error', $error->getMessage());
        }

        return $form->createView();
    }

    /**
     * @return Response
     * @Route("/user/activate", name="user_activate")
     * @Security(expression="has_role('ROLE_USER')")
     */
    public function activateAction()
    {
        /** @var User $user */
        $user = $this->getUser();
        if (!$user->hasRole('ROLE_INACTIVE')) {
            $this->addFlash('info', 'Вы уже активированы!');
            return $this->redirectToRoute('profile');
        }
        if ($user->getBalance() < 10) {
            $this->addFlash('error', 'Недостаточно средств для активации!');
            return $this->redirectToRoute('profile');
        }
        $this->activateUser();
        $this->addFlash('success', 'Вы успешно активировали свой аккаунт, поздравляем!');
        return $this->redirectToRoute('profile');
    }

    /**
     * @param $amount
     */
    private function activateUser($amount = 10)
    {
        /** @var User $user */
        $user = $this->getUser();
        $trinar = new TrinarNode();
        $trinar->setUser($user);
        $payment = new Payment();
        $payment
            ->setUser($user)
            ->setAmount($amount)
            ->setBankAccount('')
            ->setType(PaymentType::OUT)
            ->setStatus(PaymentStatus::SUCCESS)
            ->setComment('Активация')
        ;
        $user
            ->setActivationDate()
            ->setBalance($user->getBalance() - $amount * 2)
            ->setAccountCost($amount);
        $trinarManager = $this->get('trinar.manager');
        $persistenceList = array_merge($trinarManager->registerReferral($user), [
            $user,
            $trinar,
            $payment
        ]);
        $persistenceList = array_merge($persistenceList, $user->addPayment($amount));
        $user
            ->removeRole('ROLE_INACTIVE')
            ->addRole('ROLE_USER');
        $this
            ->persistList($persistenceList)
            ->flush();
        $this->getDoctrine()->getManager()->refresh($user);
        $newToken = new UsernamePasswordToken($user, null, 'main', $user->getRoles());
        $this->get('security.context')->setToken($newToken);
    }

    private function processPaymentForm(Request $request)
    {
        $payment = new Payment();
        $payment->setType(PaymentType::OUT);
        $payment->setUser($this->getUser());
        $paymentOutForm = $this->createForm(new PaymentOutFormType(), $payment, [
            'balance' => $this->getUser()->getBalance(),
        ]);
        $paymentOutForm->handleRequest($request);
        $hasErrors = false;
        if($paymentOutForm->isValid()) {
            $user = $this->getUser();
            $user->setBalance($user->getBalance() - $payment->getAmount());
            $this
                ->persistList([$user, $payment])
                ->flush();
            $this->addFlash('success', 'Заявка на вывод принята к обработке.');
        } else {
            $hasErrors = $paymentOutForm->isSubmitted();
        }
        return [
            'out_form' => $paymentOutForm->createView(),
            'formErrors' => $hasErrors,
        ];
    }

}
