<?php

namespace AppBundle\Controller;

use AppBundle\Entity\House;
use AppBundle\Entity\InventoryItem;
use AppBundle\Entity\User;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Component\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Site's house controller
 * @package AppBundle\Controller
 * @Route("/house")
 * @Security(expression="has_role('ROLE_USER')")
 */
class HouseController extends Controller
{

	/**
	 * @Route("/purchases", name="purchases")
	 */
	public function purchasesAction()
	{
		return $this->render('house/purchases.html.twig', ['typedHouses' => $this->getUser()->getTypedHouseCount(false)]);
	}

	/**
	 * @Route("/list", name="houseList")
	 */
	public function listAction()
	{
		/** @var User $user */
		$user = $this->getUser();
		$houses = $user->getHouses();
		return $this->render('house/list.html.twig', ['houses' => $houses]);
	}

	/**
	 * @Route("/view/{id}", requirements={"id"="\d+"}, name="houseView")
	 */
	public function viewAction(House $house)
	{
		if ($house->getOwner() !== $this->getUser()) {
			$this->addFlash('danger', 'Вы не можете просматривать не свои владения!');
			return $this->redirectToRoute('profile');
		}
		$inventory = $this->getDoctrine()->getManager()
			->getRepository('AppBundle:InventoryItem')
			->getUserInventory($this->getUser());
		return $this->render('house/view.html.twig', ['house' => $house, 'inventory' => $inventory]);
	}

	/**
	 * @param House $house
	 * @param Request $request
	 * @param InventoryItem $item
	 * @return Response
	 * @Route(
	 *     "/{houseId}/bundle/{itemId}",
	 *     requirements={"houseId"="\d+", "itemId"="\d+"},
	 *     options={"expose"=true},
	 *     name="bundleItemToHouse"
	 * )
	 * @ParamConverter(name="house", class="AppBundle:House", options={"id" = "houseId"})
	 * @ParamConverter(name="item", class="AppBundle:InventoryItem", options={"id" = "itemId"})
	 */
	public function bundleItemAction(House $house, InventoryItem $item, Request $request)
	{
		$x = $request->get('x');
		$y = $request->get('y');
		if ($house->getOwner() !== $this->getUser() || ! isset($x) || !isset($y)) {
			throw $this->createAccessDeniedException();
		}
		if ($item->getHouse() !== null) {
			$this->addFlash('danger', 'Этот предмет уже используется!');
		} elseif ($house->hasItemOn($x, $y)) {
			$this->addFlash('danger', 'Ячейка уже занята!');
		} else {
			$item->setX($x);
			$item->setY($y);
			$item->setHouse($house);
			$this->persistList([$item, $house])->flush();
			$this->addFlash('success', 'Предмет успешно добавлен!');
		}
		return $this->redirectToRoute('houseView', ['id' => $house->getId()]);
	}

	/**
	 * @param InventoryItem $item
	 * @return Response
	 * @Route("/unbundle/{id}", requirements={"id" = "\d+"}, name="unbundleItem", options={"expose" = true})
	 */
	public function unbundleItem(InventoryItem $item)
	{
		if ($item->getUser() !== $this->getUser()) {
			throw $this->createAccessDeniedException();
		}
		$house = $item->getHouse();
		$this->persistList([$item->setHouse(), $house])->flush();
		$this->addFlash('success', 'Предмет удалён и помещён в инвентарь!');
		return $this->redirectToRoute('houseView', ['id' => $house->getId()]);
	}
}

