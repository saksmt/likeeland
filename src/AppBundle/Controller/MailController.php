<?php

namespace AppBundle\Controller;

use AppBundle\Entity\House;
use AppBundle\Entity\HouseMessage;
use AppBundle\Entity\User;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Component\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Site's house controller
 * @package AppBundle\Controller
 * @Route("/house/mail")
 * @Security(expression="has_role('ROLE_USER')")
 */
class MailController extends Controller
{
	/**
	 * @Route("/{id}", name="listMail", requirements={"id"="\d+"})
	 */
	public function indexAction(House $house)
	{
		if ($house->getOwner() !== $this->getUser()) {
			throw $this->createAccessDeniedException();
		}
		$messages = $house->getMessages();
		return $this->render('mail/list.html.twig', ['messages' => $messages]);
	}

	/**
	 * @param HouseMessage $message
	 * @Route("/view/{id}", name="viewMail", requirements={"id"="\d+"})
	 */
	public function viewAction(HouseMessage $message)
	{
		/** @var User $user */
		$user = $this->getUser();
		if ($message->getHouse()->getOwner() !== $user) {
			throw $this->createAccessDeniedException();
		}

		if (!$message->getStatus()) {
			$message->setStatus(true);
			$user->setCoins($user->getCoins() + 1);
			$this->persistList([$user, $message])->flush();
		}

		return $this->render('mail/view.html.twig', ['message' => $message]);
	}
}

