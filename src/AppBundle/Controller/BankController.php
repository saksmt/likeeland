<?php

namespace AppBundle\Controller;

use AppBundle\Entity\User;
use AppBundle\Exception\InsufficientFundsException;
use AppBundle\Form\Type\ExchangeFormType;
use AppBundle\Form\Type\SetExchangeRatesFormType;
use AppBundle\Util\Enum\ExchangeRate;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Component\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Exchange controller
 * @package AppBundle\Controller
 * @Route("/bank")
 * @Security("has_role('ROLE_USER')")
 */
class BankController extends Controller
{
    /**
     * @param Request $request
     * @Route("/", name="bankIndex")
     * @return Response
     */
    public function indexAction(Request $request)
    {
        $erm = $this->get('exchange.manager');
        $rates = $erm->getExchangeRates();
        $form = $this->createForm(new ExchangeFormType());
        $form->handleRequest($request);
        if ($form->isValid()) {
            $data = $form->getData();
            foreach($data as $exchangeGroup => $value) {
                if ($value !== null) {
                    $groups = explode('-', $exchangeGroup);
                    try {
                        $this->persistList([
                            $erm
                                ->setUser($this->getUser())
                                ->setAmount($value)
                                ->setSource($groups[0])
                                ->setTarget($groups[1])
                                ->exchange()
                        ])->flush();
                    } catch (InsufficientFundsException $e) {
                        $this->addFlash('danger', 'Недостаточно средств для проведения операции!');
                    }
                }
            }
            $this->addFlash('success', 'Конвертация прошла успешно!');
            $this->redirectToRoute('bankIndex');
        }
        return $this->render('bank/index.html.twig', ['rates' => $rates, 'form' => $form->createView()]);
    }

    /**
     * @Route("/admin", name="bankAdmin")
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function adminAction(Request $request)
    {
        $erm = $this->get('exchange.manager');
        $rates = [
            ExchangeRate::POINTS . '-' . ExchangeRate::COINS => $erm->getRate(ExchangeRate::POINTS, ExchangeRate::COINS),
            ExchangeRate::COINS . '-' . ExchangeRate::MONEY => $erm->getRate(ExchangeRate::COINS, ExchangeRate::MONEY),
            ExchangeRate::MONEY . '-' . ExchangeRate::COINS => $erm->getRate(ExchangeRate::MONEY, ExchangeRate::COINS),
        ];
        $form = $this->createForm(new SetExchangeRatesFormType(), $rates);
        $form->handleRequest($request);
        if ($form->isValid()) {
            $erm->setExchangeRates($form->getData());
            $this->addFlash('success', 'Курс обмена обновлен!');
            return $this->redirectToRoute('bankAdmin');
        }
        return $this->render('bank/admin.html.twig', ['form' => $form->createView()]);
    }
}
