<?php

namespace AppBundle\Controller;

use AppBundle\Entity\InventoryItem;
use AppBundle\Entity\Item;
use AppBundle\Exception\InsufficientFundsException;
use Component\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class ItemShopController
 * @package AppBundle\Controller
 * @Route("/shop")
 * @Security(expression="has_role('ROLE_USER')")
 */
class ItemShopController extends Controller
{
    /**
     * @Route("/{page}", requirements={"page"="\d+"}, defaults={"page"=1}, name="itemShop")
     * @param integer $page
     * @return Response
     */
    public function listAction($page)
    {
        $items = $this->get('knp_paginator')->paginate(
            $this->getDoctrine()->getManager()->getRepository('AppBundle:Item')->findAll(),
            $page,
            9
        );
        return $this->render('shop/list.html.twig', ['items' => $items]);
    }

    /**
     * @Route("/buy/{id}", requirements={"id"="\d+"}, name="buyItem")
     * @param Item $item
     * @return Response
     */
    public function buy(Item $item)
    {
        $user = $this->getUser();
        try {
            $bought = $user->buyItem($item);
            $this->persistList([$bought, $user])->flush();
            $this->addFlash('success', 'Поздравляем с покупкой');
        } catch (InsufficientFundsException $e) {
            $this->addFlash('danger', 'Недостаточно средств для покупки!');
        }
        return $this->redirectToRoute('itemShop');
    }
}