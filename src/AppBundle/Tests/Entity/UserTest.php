<?php

namespace AppBundle\Tests\Entity;

use AppBundle\Entity\TrinarNode;
use AppBundle\Entity\User;

class UserTest extends \PHPUnit_Framework_TestCase
{

    /**
     * @var TrinarNode
     */
    private $trinar;

    /**
     * @var User
     */
    private $rootUser;

    /**
     * @var int[]
     */
    private $levelMap = [];

    /**
     * @var User[]
     */
    private $userMap = [];

    public function testPayment()
    {
        $this->generateTestData(16000);
        foreach ($this->levelMap as $userId => $level) {
            if ($level > 5) {
                continue;
            }
            $user = $this->userMap[$userId];
            $amount = rand(1, 1000);
            $previousBalance = $this->rootUser->getBalance();
            $user->addPayment($amount);
            $this->assertEquals(
                $previousBalance + $amount * User::getPercents()[$level] * $this->rootUser->getAccountMultiplier(),
                $this->rootUser->getBalance(),
                sprintf('%d - %d = %d (%d%%[%d], x%d)',
                    $this->rootUser->getBalance(),
                    $previousBalance,
                    $this->rootUser->getBalance() - $previousBalance,
                    User::getPercents()[$level] * 100,
                    $level,
                    $this->rootUser->getAccountMultiplier()
                )
            );
        }
    }

    private function generateTestData($userCount = 10)
    {
        /**
         * $builder->add($key, $type, $options)
         * $form->getData() // [$key => $userData]
         */
        $this->trinar = new TrinarNode();
        $this->rootUser = new User();
        $this->rootUser->setAccountCost(rand(10, 1000));
        $this->trinar->setUser($this->rootUser);
        for ($i = 0; $i < $userCount; $i++) {
            $user = new User();
            $user->setAccountCost(rand(10, 1000));
            $trinar = new TrinarNode();
            $trinar->setUser($user);
            $this->trinar->addChild($trinar);
            $this->userMap[$i] = $user;
            $this->setLevel($trinar, $i);
        }
    }

    private function setLevel(TrinarNode $trinar, $i)
    {
        $this->levelMap[$i] = $this->trinar->getLevelOf($trinar);
    }
}