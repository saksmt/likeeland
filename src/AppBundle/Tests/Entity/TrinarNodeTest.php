<?php

namespace AppBundle\Tests\Entity;

use AppBundle\Tests\Entity\TestableTrinarNode as TrinarNode;

class TrinarNodeTest extends \PHPUnit_Framework_TestCase
{
    private $flatStorage = [];

    public function test()
    {
        $this->iniSet('memory_limit', -1);
        $data = $this->generateTestData(9);
        $testable = $this->flatten($data->real);
        foreach ($data->flat as $levelNumber => $level) {
            $this->assertArrayHasKey($levelNumber, $testable, 'Searched level doesn\'t exists!');
            $testableLevel = $testable[$levelNumber];
            foreach ($level as $nodeId => $node) {
                $this->assertArrayHasKey($nodeId, $testableLevel, 'Searched node doesn\'t exists!');
                $testableNode = $testableLevel[$nodeId];
                $this->assertEquals($node, $testableNode, 'Trinar is not valid!');
            }
        }
    }

    protected function generateTestData($levels = TrinarNode::NODE_CHILD_LEVELS_MAX)
    {
        $trinar = new TrinarNode();
        $flatTrinar = [];
        $id = 0;
        for ($level = 0; $level < $levels; $level++) {
            $trinarLevel = [];
            $max = pow(TrinarNode::NODE_CHILDREN_MAX, $level+1);
            for ($nodeId = 0; $nodeId < $max; $nodeId++) {
                $trinarNode = (new TrinarNode())
                    ->setId($id++)
                    ->setName($level . ':' . $nodeId);
                $trinar->addChild($trinarNode);
                $trinarLevel[] = $trinarNode;
            }
            $flatTrinar[] = $trinarLevel;
        }
        return (object)['flat' => $flatTrinar, 'real' => $trinar];
    }

    protected function flatten(TrinarNode $trinarNode, $level = 0)
    {
        if (!$level) {
            $this->flatStorage = [];
        }
        foreach ($trinarNode->getChildren() as $child) {
            $this->setRendered($level, $child);
            $this->flatten($child, $level+1);
        }
        return $this->flatStorage;
    }

    protected function setRendered($level, $child)
    {
        if (!isset($this->flatStorage[$level])) {
            $this->flatStorage[$level] = [];
        }
        $this->flatStorage[$level][] = $child;
    }

}