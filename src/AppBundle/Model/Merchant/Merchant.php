<?php

namespace AppBundle\Model\Merchant;

use Doctrine\Bundle\DoctrineBundle\Registry;
use Symfony\Component\HttpFoundation\ParameterBag;

class Merchant
{
    private static $hashComponents = [
        'PAYMENT_ID',
        'PAYEE_ACCOUNT',
        'PAYMENT_AMOUNT',
        'PAYMENT_UNITS',
        'PAYMENT_BATCH_NUM',
        'PAYER_ACCOUNT',
        'MD5',
        'TIMESTAMPGMT'
    ];
    /**
     * @var string
     */
    private $key;

    /**
     * @var Registry
     */
    private $doctrine;

    /**
     * @param string $secretKey
     * @param Registry $doctrine
     */
    public function __construct($secretKey, Registry $doctrine)
    {
        $this->key = strtoupper(md5($secretKey));
        $this->doctrine = $doctrine;
    }

    /**
     * @param ParameterBag $postData
     * @param callable $callback Callback with parameters @a $user & @a $amount
     * @return bool
     */
    public function verify(ParameterBag $postData, callable $callback)
    {
        $user = $this->doctrine->getRepository('AppBundle:User')
            ->find($postData->get('PAYMENT_ID'));
        $preHash = $this->buildHash($postData);
        if (isset($user) && $preHash == $postData->get('V2_HASH')) {
            $callback($user, floatval($postData->get('PAYMENT_AMOUNT')), $postData->get('PAYER_ACCOUNT'));
            return true;
        } else {
            return false;
        }
    }

    /**
     * @param ParameterBag $postData
     * @return string
     */
    private function buildHash(ParameterBag $postData)
    {
        $postData->set('MD5', $this->key);
        $preHash = '';
        foreach (static::$hashComponents as $hashKey) {
            $preHash .= $postData->get($hashKey) . ':';
        }
        $preHash = strtoupper(md5(substr($preHash, 0, -1)));
        return $preHash;
    }
}