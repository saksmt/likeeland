<?php

namespace AppBundle\Exception;

class InsufficientFundsException extends \Exception
{
}