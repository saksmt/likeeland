<?php

namespace AppBundle\Repository;

use AppBundle\Entity\User;
use Doctrine\ORM\EntityRepository;

class UserRepository extends EntityRepository
{
    /**
     * @return User[]
     */
    public function findActive()
    {
        return $this->findByNotRole('ROLE_INACTIVE');
    }

    public function findInactive()
    {
        return $this->findByRole('ROLE_INACTIVE');
    }

    public function findByRole($role)
    {
        $qb = $this->_em->createQueryBuilder();
        $qb->select('u')
            ->from($this->_entityName, 'u')
            ->where('u.roles LIKE :roles')
            ->setParameter('roles', '%"' . $role . '"%');
        return $qb->getQuery()->getResult();
    }

    public function findAllWithAvatar()
    {
        $qb = $this->_em->createQueryBuilder();
        $qb->select('u')
            ->from($this->_entityName, 'u')
            ->where('u.avatarPath is not null');
        return $qb->getQuery()->getResult();
    }

    public function findByNotRole($role)
    {
        $qb = $this->_em->createQueryBuilder();
        $qb->select('u')
            ->from($this->_entityName, 'u')
            ->where('NOT(u.roles LIKE :roles)')
            ->setParameter('roles', '%"' . $role . '"%');
        return $qb->getQuery()->getResult();
    }

    public function findNotBundled()
    {
        $qb = $this->_em->createQueryBuilder();
        $qb->select('u')
            ->from($this->_entityName, 'u')
            ->where('NOT(u.roles LIKE :roles)')
            ->andWhere('u.trinar IS NULL')
            ->setParameter('roles', '%"ROLE_INACTIVE"%');
        return $qb->getQuery()->getResult();
    }
}