<?php

namespace AppBundle\Repository;

use AppBundle\Entity\Deposit;
use Doctrine\ORM\EntityRepository;

class DepositRepository extends EntityRepository
{
    /**
     * @return Deposit[]
     */
    public function findAllNeedPayment()
    {
        return $this->createQueryBuilder('d')
            ->select('d')
            ->join('d.investor', 'i')
            ->where('d.paid = 0')
            ->andWhere('d.endDate <= CURRENT_TIMESTAMP()')
            ->getQuery()
            ->getResult();
    }
}