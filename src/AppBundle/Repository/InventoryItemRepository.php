<?php

namespace AppBundle\Repository;

use AppBundle\Entity\User;
use Doctrine\ORM\EntityRepository;

class InventoryItemRepository extends EntityRepository
{
    public function getUserInventory(User $user)
    {
        return $this->createQueryBuilder('i')
            ->select('i')
            ->join('i.item', 'ii')
            ->where('i.house IS NULL')
            ->andWhere('i.user = :user')
            ->getQuery()
            ->setParameter('user', $user->getId())
            ->getResult()
        ;
    }
}