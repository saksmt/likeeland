<?php

namespace AppBundle\Repository;

use AppBundle\Entity\Payment;
use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\EntityRepository;

/**
 * Payment repository
 * @package AppBundle\Repository
 */
class PaymentRepository extends EntityRepository
{
    /**
     * Find payments by criteria with joined users
     * @param array $criteria
     * @return Payment[]
     */
    public function findWithUserBy(array $criteria)
    {
        return $this->getEntityManager()->createQueryBuilder()
            ->select('p')
            ->addSelect('u')
            ->from('AppBundle:Payment', 'p')
            ->join('p.user', 'u')
            ->addCriteria($this->convertArrayToCriteria($criteria))
            ->getQuery()
            ->getResult()
        ;
    }

    /**
     * Convert simple field-array associated criteria to @a Criteria object
     * @param array $criteria
     * @return Criteria
     */
    private function convertArrayToCriteria(array $criteria)
    {
        $result = Criteria::create();
        foreach ($criteria as $field => $value) {
            $result->andWhere($result->expr()->eq($field, $value));
        }
        return $result;
    }
}