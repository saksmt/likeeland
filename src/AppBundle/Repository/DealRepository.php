<?php

namespace AppBundle\Repository;

use AppBundle\Entity\HouseType;
use AppBundle\Entity\User;
use AppBundle\Util\Enum\PaymentStatus;
use Doctrine\ORM\EntityRepository;

class DealRepository extends EntityRepository
{
    public function findActiveByType(HouseType $houseType)
    {
        return $this->queryActiveByType($houseType)->getResult();
    }

    public function findActiveByUser(User $user)
    {
        return $this->queryActiveByUser($user)->getResult();
    }

    public function findAllActive()
    {
        return $this->queryAllActive()->getResult();
    }

    /**
     * @return \Doctrine\ORM\AbstractQuery
     */
    public function queryAllActive()
    {
        return $this->createQueryBuilder('d')
            ->select('d')
            ->where('d.status = :status')
            ->getQuery()
            ->setParameter('status', PaymentStatus::PENDING);
    }

    /**
     * @param User $user
     * @return \Doctrine\ORM\AbstractQuery
     */
    public function queryActiveByUser(User $user)
    {
        return $this->createQueryBuilder('d')
            ->select('d')
            ->where('d.seller = :user')
            ->andWhere('d.status = :status')
            ->getQuery()
            ->setParameter('user', $user)
            ->setParameter('status', PaymentStatus::PENDING);
    }

    /**
     * @param HouseType $houseType
     * @return \Doctrine\ORM\AbstractQuery
     */
    public function queryActiveByType(HouseType $houseType)
    {
        return $this->createQueryBuilder('d')
            ->select('d')
            ->where('EXISTS(SELECT hh FROM AppBundle:House hh WHERE hh.type = :type AND hh MEMBER OF d.houses)')
            ->andWhere('d.status = :status')
            ->getQuery()
            ->setParameter('type', $houseType->getId())
            ->setParameter('status', PaymentStatus::PENDING);
    }
}