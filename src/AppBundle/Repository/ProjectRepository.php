<?php

namespace AppBundle\Repository;

use AppBundle\Entity\Project;
use AppBundle\Util\Enum\ProjectStatus;
use Doctrine\ORM\AbstractQuery;
use Doctrine\ORM\EntityRepository;

class ProjectRepository extends EntityRepository
{
    /**
     * @param bool $loadPending
     * @return array
     */
    public function findAllAccepted($loadPending = false)
    {
        return $this->queryAllAccepted($loadPending)->getResult();
    }

    /**
     * @param bool $loadPending
     * @return AbstractQuery
     */
    public function queryAllAccepted($loadPending = false)
    {
        return $this->createQueryBuilder('p')
            ->select('p')
            ->where('p.status NOT IN(:notAccepted)')
            ->getQuery()
            ->setParameter('notAccepted', array_merge([
                ProjectStatus::DECLINED,
            ], $loadPending ? [] : [ProjectStatus::PENDING]))
        ;
    }

    public function queryAllOutdated()
    {
        return $this->createQueryBuilder('p')
            ->select('p')
            ->where('DATE_ADD(p.reviewDate, p.projectTime, \'DAY\') < CURRENT_DATE()')
            ->getQuery()
        ;
    }

    /**
     * @return Project[]
     */
    public function findAllOutdated()
    {
        return $this->queryAllOutdated()->getResult();
    }
}