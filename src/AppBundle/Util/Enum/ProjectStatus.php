<?php

namespace AppBundle\Util\Enum;

/**
 * Enumeration of statuses for @a Project
 * @package AppBundle\Util\Enum
 */
abstract class ProjectStatus extends AbstractEnum
{
    /**
     * @const Succeed project
     */
    const SUCCEED = 0;

    /**
     * @const Failed project
     */
    const FAILED = 1;

    /**
     * @const Project needing review
     */
    const PENDING = 2;

    /**
     * @const Project accepted by review
     */
    const ACCEPTED = 3;

    /**
     * @const Project rejected by review
     */
    const DECLINED = 4;
}