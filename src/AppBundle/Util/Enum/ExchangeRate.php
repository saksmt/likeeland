<?php

namespace AppBundle\Util\Enum;

/**
 * Exchange rate types
 * @package AppBundle\Util\Enum
 */
abstract class ExchangeRate extends AbstractEnum
{
    /**
     * @const string Money type
     */
    const MONEY = 'balance';

    /**
     * @const string Points type
     */
    const POINTS = 'points';

    /**
     * @const string Coins type
     */
    const COINS = 'coins';
}