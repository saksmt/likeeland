<?php

namespace AppBundle\Util\Enum;

abstract class AbstractEnum
{
    protected static $myConstants;

    public static function getConstantName($value)
    {
        $constants = static::getAll();
        return array_search($value, $constants);
    }

    public static function getAll()
    {
        if (!isset(static::$myConstants)) {
            $reflection = new \ReflectionClass(static::class);
            static::$myConstants = $reflection->getConstants();
        }
        return static::$myConstants;
    }
}