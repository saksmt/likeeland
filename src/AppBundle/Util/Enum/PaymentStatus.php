<?php

namespace AppBundle\Util\Enum;

/**
 * Payment statuses
 * @package AppBundle\Util\Enum
 */
abstract class PaymentStatus extends AbstractEnum
{
    /**
     * @const int Pending payment
     */
    const PENDING = 2;

    /**
     * @const int Done with success payment
     */
    const SUCCESS = 1;

    /**
     * @const int Done with failure payment
     */
    const FAIL = 0;
}