<?php

namespace AppBundle\Util\Enum;

/**
 * Payment types
 * @package AppBundle\Util\Enum
 */
abstract class PaymentType extends AbstractEnum
{
    /**
     * @const int "in" payment, refill
     */
    const IN = 0;

    /**
     * @const int "out" payment, withdrawal
     */
    const OUT = 1;
}