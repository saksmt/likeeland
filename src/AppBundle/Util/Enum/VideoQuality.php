<?php

namespace AppBundle\Util\Enum;

abstract class VideoQuality extends AbstractEnum
{
    const Q240 = 0;
    const Q360 = 1;
    const Q480 = 2;
    const Q720 = 3;
}