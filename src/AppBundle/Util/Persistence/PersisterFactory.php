<?php

namespace AppBundle\Util\Persistence;

use Doctrine\Common\Persistence\ObjectManager;

class PersisterFactory
{
    /**
     * @var ObjectManager
     */
    private $om;

    public function __construct(ObjectManager $om)
    {
        $this->om = $om;
    }

    /**
     * @param int $packetCount
     * @return Persister
     */
    public function createPersister($packetCount = 100)
    {
        return Persister::createPersister($this->om, $packetCount);
    }
}