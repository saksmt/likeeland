<?php

namespace AppBundle\Util\Persistence;

use Doctrine\Common\Persistence\ObjectManager;

class Persister
{
    private $packetCount = 100;
    private $counter = 0;

    /**
     * @var ObjectManager
     */
    private $em;

    private function __construct($packetCount = 100)
    {
        $this->packetCount = $packetCount;
    }

    /**
     * @param ObjectManager $om
     * @param int $packetCount
     * @return Persister
     */
    public static function createPersister(ObjectManager $om, $packetCount = 100)
    {
        $me = new static($packetCount);
        $me->em = $om;
        return $me;
    }

    public function persist($entity)
    {
        $this->em->persist($entity);
        if ($this->counter++ > $this->packetCount) {
            $this->em->flush();
            $this->counter = 0;
            return true;
        }
        return false;
    }

    public function persistMany(array $entityList)
    {
        $last = array_pop($entityList);
        foreach ($entityList as $entity) {
            $this->em->persist($entity);
        }
        return $this->persist($last);
    }

    public function flush()
    {
        if ($this->counter > 0) {
            $this->em->flush();
            return true;
        }
        return false;
    }
}