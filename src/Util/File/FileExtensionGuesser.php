<?php

namespace Util\File;

use Symfony\Component\HttpFoundation\File\UploadedFile;

class FileExtensionGuesser
{
    private $file;
    private $extension = null;
    private $currentIndex = 0;
    private static $methodList = [
        'guessClientExtension',
        'guessExtension',
        'getExtension',
        'getClientExtension',
    ];

    public function __construct(UploadedFile $file)
    {
        $this->file = $file;
    }

    public function getExtension($default = null)
    {
        while (($extension = $this->parseExt()) === null);
        if (!$this->isParsed()) {
            return $default;
        }
        return $this->parseExt();
    }

    private function isParsed()
    {
        return isset($this->extension) && !empty($this->extension);
    }

    private function parseExt()
    {
        if (!isset($this->extension)) {
            if ($this->currentIndex < count(self::$methodList)) {
                $extension = $this->file->{self::$methodList[$this->currentIndex++]}();
                $this->extension = $extension;
                return $extension;
            }
            $this->extension = '';
            return '';
        }
        return $this->extension;
    }
}