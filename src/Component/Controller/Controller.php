<?php

namespace Component\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller as BaseController;
use Symfony\Component\HttpFoundation\Response;

class Controller extends BaseController
{
    protected function persistList($list)
    {
        $em = $this->getDoctrine()->getManager();
        foreach ($list as $element) {
            $em->persist($element);
        }
        return $em;
    }
}