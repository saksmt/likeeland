(function (angular, console) {
    'use strict';
    angular.module('loggerModule', [])
        .provider('logger', function () {
            var
                logLevels = [
                    'debug',
                    'info',
                    'warning'
                ],
                logLevel = 'info',
                checkLevel = function (level) {
                    return logLevels.indexOf(level) <= logLevels.indexOf(logLevel);
                };
            this.setLogLevel = function (level) {
                if (logLevels.indexOf(level) > 0) {
                    logLevel = level;
                }
            };
            this.logError = function () {
                console.error.apply(console, arguments);
            };
            this.logWarning = function () {
                if (checkLevel('warning')) {
                    console.warn.apply(console, arguments);
                }
            };
            this.logInfo = function () {
                if (checkLevel('info')) {
                    console.info.apply(console, arguments);
                }
            };
            this.logDebug = function () {
                if (checkLevel('debug')) {
                    console.debug.apply(console, arguments);
                }
            };
            this.$get = this;
        });
}(window.angular, window.console));