(function (angular) {
    'use strict';
    angular.module('messagesModule', ['moduleLoader', 'translationModule'])
        .service('messages', ['translator', function (translator) {
            // Private variables
            var storage = [],
                defaultDismissible = true,
                messageTypes = [
                    'success',
                    'danger',
                    'warning',
                    'info',
                    'error',
                    'debug'
                ],
                type,
                messages = this;
            // Subclass
            function Message(type, text, dismissible) {
                var me = {},
                    convertableTypes = {
                        'error': 'danger',
                        'debug': 'info'
                    },
                    allowedTypes = [
                        'danger',
                        'warning',
                        'success',
                        'info'
                    ];
                // Definition
                this.getTwbsClass = function () {
                    return me.class;
                };
                this.getContent = function () {
                    return me.content;
                };
                this.setDismissible = function () {
                    if (!me.dismissible) {
                        me.class += ' alert-dismissible'
                    }
                };
                this.setContent = function (content) {
                    me.content = content;
                };
                this.setType = function (type) {
                    me.type = type;
                    if (convertableTypes.hasOwnProperty(me.type)) {
                        me.type = convertableTypes[type];
                    }
                    if (allowedTypes.indexOf(me.type) < 0) {
                        throw 'Message type "' + type + '" is not allowed!';
                    }
                    me.class = 'alert alert-' + me.type;
                    if (me.dismissible) {
                        me.class += ' alert-dismissible';
                    }
                };
                this.isDismissible = function () {
                    return !!me.dismissible;
                };
                // Initialization
                me.content = text;
                this.setType(type);
                if (dismissible) {
                    me.dismissible = true;
                    me.class += ' alert-dismissible';
                }
            }
            // Private methods
            function getMethodName(type) {
                var methodName = messageTypes[type];
                methodName = methodName.charAt(0).toUpperCase() + methodName.slice(1);
                return methodName;
            }
            function getAddName(type) {
                return 'add' + getMethodName(type);
            }
            function createAddFunction(type) {
                return function (messageText, isDismissible) {
                    messages.add(type, messageText, isDismissible);
                };
            }
            // Public methods
            this.add = function (messageType, messageText, isDismissible) {
                if (isDismissible === undefined) {
                    isDismissible = defaultDismissible;
                }
                storage.push(new Message(messageType, messageText, isDismissible));
            };
            this.addSuccessAction = function (actionName, isDismissible) {
                var message = translator.translate('success.action').format({ action: actionName });
                this.addSuccess(message, isDismissible);
            };
            this.addErrorMessage = function (isDismissble) {
                this.add('error', translator.translate('error.message'), isDismissble);
            };
            this.pop = function () {
                if (storage.length) {
                    return storage.pop();
                }
                return false;
            };
            this.setDefaultDismissible = function (dissmissible) {
                defaultDismissible = dissmissible;
                return this;
            };
            // Aliases
            for (type in messageTypes) {
                if (messageTypes.hasOwnProperty(type)) {
                    this[getAddName(type)] = createAddFunction(messageTypes[type]);
                }
            }
        }])
        .config(['moduleProvider', function (moduleProvider) {
            moduleProvider
                .controller('messageController', ['messages', function (messages) {
                    this.poll = function () {
                        var message = messages.pop();
                        if (message !== false) {
                            this.messages.push(message);
                        }
                    };
                    this.get = function () {
                        return messages.pop();
                    };
                    this.messages = [];
                }]);
        }]);
}(window.angular));