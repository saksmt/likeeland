var register;
(function (angular) {
    'use strict';
    register = function (moduleName) {
        if (app.requires.indexOf(moduleName) < 0) {
            app.requires.push(moduleName);
        }
    };
    angular.module('moduleLoader', ['loggerModule'])
        .provider('module', [
            '$compileProvider',
            '$controllerProvider',
            'loggerProvider',
            function ($compileProvider, $controllerProvider, loggerProvider) {
                var providers = {
                    compiler: $compileProvider,
                    controller: $controllerProvider
                };
                this.setCompilerProvider = function (complierProvider) {
                    loggerProvider.logInfo('Initiated compiler!');
                    providers.compiler = complierProvider;
                    return this;
                };
                this.setControllerProvider = function (controllerProvider) {
                    loggerProvider.logInfo('Initiated controller!');
                    providers.controller = controllerProvider;
                    return this;
                };
                this.controller = function (controllerName, controllerConstructor) {
                    loggerProvider.logInfo('Registered controller "' + controllerName + '"!');
                    providers.controller.register(controllerName, controllerConstructor);
                    return this;
                };
                this.directive = function (directiveName, directiveConstructor) {
                    loggerProvider.logInfo('Registered directive "' + directiveName + '"!');
                    providers.compiler.directive(directiveName, directiveConstructor);
                    return this;
                };
                this.$get = angular.noop;
            }]);
}(window.angular));
