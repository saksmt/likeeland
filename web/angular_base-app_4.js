var app;
(function (angular) {
    'use strict';
    app = angular.module('app', [
        'moduleLoader'
    ]);
}(window.angular));
