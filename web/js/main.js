/*
ЁБАНЫЙВРОТ!!!БЛЯТЬ ЭТОЧТО???
 * javascript funtions for application
 */

$(document).ready(function() {
    // houses popup load
    $('body').delegate('.house_list_wrap ul li a, .house_list_wrap ul li button', 'click', function() {
	if ($('.houses-modal').length > 0) {
	    $('.houses-modal').remove();
	}
	
	var id = $(this).attr('data-id');
	var quantity = $(this).attr('data-quantity');
	
	$.ajax({
	    url: '/house/popup',
	    type: 'post',
	    data: { id: id, quantity: quantity },
	    dataType: 'html',
	    success: function(html) {
		if (html) {
		    $('.panel-body').append(html);
		}
	    },
	    complete: function() {
		$('.house_list_wrap .btn.btn-link').trigger('click');
	    }
	});
    });
    // end houses popup load
    //buy house
    $('body').delegate('.houses-modal .buy_house', 'click', function() {
	$.ajax({
	    url: '/house/buy',
	    type: 'post',
	    data: $('.houses-modal input, .houses-modal select'),
	    dataType: 'json',
	    success: function(json) {
		if (json.error) {
		    $('.houses-modal .modal-content .alert p').remove();
		    $('.houses-modal .modal-content .alert').append(json.error).show();
		}
		if (json.success) {
		    $('.houses-modal .modal-content').html('<p>' + json.success + '</p>');
		    setTimeout(function() {
			$('.fade').trigger('click');
		    }, 2000);
		    
		    $.ajax({
			url: '/house/upadate_info',
			type: 'post',
			dataType:  'html',
			success: function(html) {
			    if (html) {
				$('.houses_content .house_list_wrap').html(html);
			    }
			}
		    });
		}
	    }
	});
    });
    //end buy house
});
