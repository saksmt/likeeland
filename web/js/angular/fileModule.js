(function (angular, register) {
    "use strict";
    angular.module('fileModule', ['moduleLoader'])
        .config(['moduleProvider', function (moduleProvider) {
            moduleProvider.directive('file', function () {
                return {
                    scope: {
                        file: '='
                    },
                    link: function (scope, el) {
                        el.bind('change', function (event) {
                            var files = event.target.files,
                                file = files[0];
                            scope.file = file ? file.name : undefined;
                            scope.$apply();
                        });
                    }
                };
            });
        }]);
    register('fileModule');
}(window.angular, window.register));