(function ($, Routing, getHouseId, alert) {
    'use strict';
    var
        currentItem = null,
        editMode = false,
        inventoryModal = $('#inventory');
    $('.inventory-item').click(function () {
        if (editMode) {
            var $this = $(this);
            currentItem = {
                image: $this.attr('src'),
                id: $this.data('id')
            };
            inventoryModal.modal().modal('hide');
        } else {
            alert('Для размещения предмета войдите в режим редактирования!');
        }
    });
    $('#editModeToggle').change(function () {
        currentItem = null;
        $('.items').toggleClass('bordered');
        $('#inventoryModalTitle').children().each(function () {
            $(this).toggle();
        });
        editMode = !editMode;
    });
    $('[data-full]').click(function () {
        if (editMode) {
            window.location.href = Routing.generate('unbundleItem', {
                id: $(this).data('id')
            });
        }
    });
    $('[data-empty]')
        .mouseover(function () {
            if (currentItem !== null) {
                $(this).css('backgroundImage', 'url("' + currentItem.image + '")');
            }
        })
        .mouseout(function () {
            $(this).css('backgroundImage', '');
        })
        .click(function () {
            if (currentItem !== null) {
                var data = $(this).data();
                window.location.href = Routing.generate('bundleItemToHouse', {
                    houseId: getHouseId(),
                    itemId: currentItem.id,
                    x: data.x,
                    y: data.y
                });
            }
        });
}(window.jQuery, window.Routing, window.getHouseId, window.alert));