(function ($, videojs) {
    'use strict';
    videojs('#video', {plugins: {resolutionSelector: {default_res: '720,480,360,240'}}}, function () {
        var BANNER_TIME = 10000,
            BANNER_CONTENTS = ['Нажмите на кнопку "закрыть" в течении 10 секунд..', 'Нажмите на кнопку "закрыть" в течении 10 секунд..'],
            bannerCount = BANNER_CONTENTS.length,
            player = this,
            isRun = false,
            durations,
            bannerClicks = 0,
            bannerButton = $("#bannerButton"),
            getRandomInt = function (min, max) {
                return Math.round(Math.round(Math.random()*max*1000) % (max - min + 1) + min);
            };

        setTimeout(function () { $(".res-changer").children().append('<span id="resolutionValue">' + player.getCurrentRes() + 'p</span>'); }, 1000);

        player
            .on('changeRes', function () {
                $("#resolutionValue").text(player.getCurrentRes() + 'p');
            })
            .on('loadedmetadata', function () {
                var duration = parseInt(player.duration());
                durations = [getRandomInt(0, duration / 2), getRandomInt(duration / 2, duration)];
                player.on('timeupdate', function () {
                    if (durations.indexOf(parseInt(player.currentTime())) !== -1 && !isRun) {
                        isRun = true;
                        player.pause();
                        $("#banner_context").text(BANNER_CONTENTS[bannerClicks]);
                        $(".banner-modal").modal();
                        var timer = setTimeout(function () {
                            window.location = '/video/fail';
                        }, BANNER_TIME);
                        bannerButton.click(function () {
                            clearTimeout(timer);
                            bannerClicks += 1;
                            player.play();
                            setTimeout(function () { isRun = false; }, 1000);
                            bannerButton.off('click');
                        });
                    }
                })
            })
            .on('ended', function () {
                console.log('video ended!');
                if (bannerClicks === bannerCount) {
                    window.location = '/video/success';
                } else {
                    window.loctaion = '/video/fail';
                }
            });
    });
}(window.jQuery, window.videojs));